import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16'; 
import { Add } from '../../components/dashboard/consumables/add';

configure({ adapter: new Adapter() });

describe('Testing Add Component', function() {
	it('state.typeOfItem should be an empty string', function() {
		const wrapper = shallow(<Add />);
		expect(wrapper.state('typeOfItem')).to.equal('');
	});

	it('state.typeOfItem should equal food when food button is clicked', function() {
		const wrapper = shallow(<Add />);
		wrapper.find('.type-of-item-button').first().simulate('click');
		expect(wrapper.state('typeOfItem')).to.equal('food');
	});

	it('state.typeOfItem should equal drink when drink button is clicked', function() {
		const wrapper = shallow(<Add />);
		wrapper.find('.type-of-item-button').last().simulate('click');
		expect(wrapper.state('typeOfItem')).to.equal('drink');
	});

});