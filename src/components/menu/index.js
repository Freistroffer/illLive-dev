import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { toggleMenu } from './menu_actions';
import { beginSigningOut } from '../user/user_actions';
import { setLoader } from '../loading/loading_actions';
import { authenticatedRoutes, notAuthenticatedRoutes } from '../../routes';
import * as styles from './styles.css';

export class Menu extends Component { // eslint-disable-line react/prefer-stateless-function
  snare = false;

  constructor(props) {
    super(props);

    this.state = {
      menuState: 'closed'
    };
  }


  signOut = () => {
    this.props.toggleMenu();
    this.props.beginSigningOut();
  }


  render() {
    const { siteName, toggleMenu } = this.props;

    const routes  = this.props.username !== "" ? authenticatedRoutes : notAuthenticatedRoutes;
    const SignOut = this.props.username !== "" ? <div className="sign-out-link" onClick={this.signOut}>Sign Out</div> : null;

    return (
      <div className={`menu ${ this.props.menuOpen ? 'open fade-out' : 'close' }`} styles={styles}>
        <div className="menu--overlay">
          <div className="menu--overlay-header flex row space-between align-center">
            <h1><Link to="/">{siteName}</Link></h1>
            <div className="close-menu" onClick={toggleMenu}>
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
          <nav>
                      {
            routes.map(( route, index ) => {
                if (route.name !== "Intro Profile") {
                  return <Link key={index + 1} to={route.path} onClick={toggleMenu}>{route.name}</Link>
                } else {
                  return undefined;
                }
            })
          }
          {SignOut}
          </nav>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    username: state.userReducers.username,
    menuOpen: state.menuReducers.menuOpen
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    beginSigningOut: () => dispatch(beginSigningOut()),
    setLoader: (isLoading = false, loadingText = "") => dispatch(setLoader(isLoading, loadingText)),
    toggleMenu: () => dispatch(toggleMenu())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
