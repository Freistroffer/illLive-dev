import React, { Component } from 'react';
import { notAuthenticatedRoutes, authenticatedRoutes } from '../../routes';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as styles from './styles.css';

export class Footer extends Component {
	render() {
		const { user } = this.props; 
		let routesToMapOver;
		let routes;

		if ( user && user.hasOwnProperty('username')  ) {
			routesToMapOver = authenticatedRoutes;
		} else {
			routesToMapOver = notAuthenticatedRoutes;
		}

		routes = routesToMapOver.map((route, index) => {
			return <Link to={ route.path } key={index + 1}>{route.name}</Link> 
		});

		return(
			<footer style={styles}>
				<section className="routes">{routes}</section>
				<section className="info">
					<ul>
						<li>Have any questions? <Link to="contact-us">Contact Us</Link></li>
						<li>Copyright 2018</li>
						<li>Created by <a href="https://www.upwork.com/fl/kevinfdev" target="_blank" rel="noopener noreferrer">Kevin Freistroffer</a></li>
					</ul>
				</section>
			</footer>
		);	
	}
}

const mapStateToProps = state => {
		return {
			user: state.userReducers
		}
};

const mapDispatchToProps = dispatch => {
	return {

	}
}


export default connect(mapStateToProps, mapDispatchToProps)(Footer);
