import React, { Component } from 'react';
import * as pageNotFoundStyles from './styles.css';

/**
 * PageNotFound
 */
export class PageNotFound extends Component {
  render() {
    return (
      <div className="page-not-found view-with-padding flex center-all" styles={pageNotFoundStyles}>
        <h1>Sorry that page doesn't exist.</h1>
      </div>
    )
  }
}

export default PageNotFound;
