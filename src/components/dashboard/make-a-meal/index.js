import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import classNames from "classnames";
import moment from "moment";
import momentz from "moment-timezone";
import * as styles from "./styles.css";
import { setAlert } from "../../alert/alert_actions";
import { setUser, confirm24HourMealResetMessage } from "../../user/user_actions";
import { saveMeals } from "../dashboardService";
import * as LocalStorage from "../../../utilities/localStorage";
import Detail from "./detail";
import Meal from "./meal";
import Clock from "react-live-clock";

const DetailStyles = {
  position: "absolute"
};

export class MakeAMeal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Make a meal",
      date: "",
      timezoneGuess: "",
      showDetailModal: false,
      servingSizes: ["1", "3/4", "1/2", "1/3", "1/4", "1/8", "1/16", "1/32"],
      newMeasurement: "",
      selectedFoodDrink: {
        quantity: 0,
        servingSize: "",
        measurement: ""
      },
      meals: [],
      selectedMeal: "",
      mealName: "",
      showMealNameInput: false,
      foodDrinkSelectValue: "Choose",
      formSubmitted: false,
      showSelectAMealMessage: false
    };

    this.quantityRef = React.createRef();

    document.title = "Make a Meal"; 
  }

  componentDidMount() {
    const { date, timezoneGuess } = this.getDateAndTimeZone();
    let meals;
    let selectedMeal;
    let mealsFromLocalStorage;
    let selectedMealFromLocalStorage;


    if (this.props.user.isAuthenticated) {
      console.log(`componentDidMount if is Authenticated`);
      try {
        mealsFromLocalStorage = LocalStorage.getItem("meals");
        if (!mealsFromLocalStorage) {
          meals = this.props.user.meals; 
        } else if (mealsFromLocalStorage && mealsFromLocalStorage.length) {
          meals = mealsFromLocalStorage;
        }
      } catch (error) {
        console.warn(
          `An error occured calling localStorage.getItem('meals')`,
          error
        );

        meals = this.props.user.meals;
      }

      try {
        selectedMealFromLocalStorage = LocalStorage.getItem("selectedMeal");
        selectedMeal = !selectedMealFromLocalStorage ? this.state.selectedMeal : selectedMealFromLocalStorage;
      } catch (error) {
        console.log(
          `An error occured calling localStorage.getItem('selectedMeal')`,
          error
        );
        selectedMeal = this.state.selectedMeal; 
      }

      this.setState({
        date,
        timezoneGuess,
        meals: [...meals], 
        selectedMeal: Object.assign({}, this.state.selectedMeal, selectedMeal)
      });
    } else {
      this.setState({
        date,
        timezoneGuess
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    let meals;
    let selectedMeal;
    let mealsFromLocalStorage;
    let selectedMealFromLocalStorage;

    // Handles if /make-a-meal is refreshed
    if (!prevProps.user.isAuthenticated && this.props.user.isAuthenticated) {
      console.log(`componentDidUpdate  isAuthenticated etc.`);
      try {
        mealsFromLocalStorage = LocalStorage.getItem("meals");
        if (!mealsFromLocalStorage) {
          meals = this.props.user.meals; 
        } else if (mealsFromLocalStorage && mealsFromLocalStorage.length) {
          meals = mealsFromLocalStorage;
        }
      } catch (error) {
        console.warn(
          `An error occured calling localStorage.getItem('meals')`,
          error
        );

        meals = this.props.user.meals;
      }

      try {
        selectedMealFromLocalStorage = LocalStorage.getItem("selectedMeal");
        selectedMeal = !selectedMealFromLocalStorage ? this.state.selectedMeal : selectedMealFromLocalStorage;
        console.log(selectedMeal);
      } catch (error) {
        console.log(
          `An error occured calling localStorage.getItem('selectedMeal')`,
          error
        );
        selectedMeal = this.state.selectedMeal; 
      }

      this.setState({
        meals: [...meals], 
        selectedMeal: Object.assign({}, this.state.selectedMeal, selectedMeal)
      }, () => console.log(this.state.selectedMeal));
    }
  }

  getDateAndTimeZone = () => {
    let dateObj = new Date();
    const momentDateObj = moment(dateObj);
    const month = momentDateObj.format("MMMM");
    const day = momentDateObj.format("DD");
    const year = momentDateObj.format("YYYY");
    const date = `${month} ${day}, ${year}`;
    const timezoneGuess = momentz.tz.guess();

    return { date, timezoneGuess };
  }

  /*
    @param foodDrink {} : foodDrink can either be a selectedFoodDrink 
                          and it's serving size and quantity, or an empty object {}
  */
  createNewMeal = (foodDrink = {}) => {
    const { meals, selectedFoodDrink } = this.state;
    let foodDrinkObjectIsNotEmpty = false;

    // Is the meal created with a foodDrink object ie { servingSize, quantity etc. }
    for(let key in foodDrink) {
        if(foodDrink.hasOwnProperty(key)) {
          foodDrinkObjectIsNotEmpty = true;
        }
    }

    //const id = _.uniqueId();
    const id = (!meals.length || meals.length < 1) ? 1 : meals.length + 1;

    if (foodDrinkObjectIsNotEmpty) {
      foodDrink.quantity = selectedFoodDrink.quantity;
    }

    const newMeal = {
      id,
      name: `Meal-${id}`,
      foodDrinks: foodDrinkObjectIsNotEmpty ? [foodDrink] : [],
      saved: false
    };

   
    this.setState(
      {
        meals: [...this.state.meals, newMeal],
        formSubmitted: false,
        selectedFoodDrink: {
          ...selectedFoodDrink,
          servingSize: "",
          quantity: ""
        },
        selectedMeal: Object.assign({}, this.state.selectedMeal, newMeal)
      },
      () => {
        LocalStorage.addToLocalStorage("meals", this.state.meals);
      }
    );
  }

  addFoodDrinkToMeal = () => {
    const { meals, selectedMeal, selectedFoodDrink } = this.state;
    const { quantity } = selectedFoodDrink;

    let mealsToUpdate = [...meals];
    let foodDrinkToUpdate = { ...selectedFoodDrink };
    let isSameServingSize;
    let foodDrinkIndex;
    let foodDrinkIsAlreadyAdded;
    let foundMeal;
    let mealIndex;

    // If no meals have been created today, than create a new meal with the foodDrink
    // and exit the function
    if (meals && meals.length < 1) {
      this.createNewMeal(selectedFoodDrink);

      // TODO make this happen after createNewMeal is finished for sure
      LocalStorage.addToLocalStorage('selectedMeal', this.state.meals[0]);

      return;
    };

    // Is the foodDrink already in the meal?
    foodDrinkIsAlreadyAdded = this.foodDrinkAlreadyExists(selectedFoodDrink);
    // If so, is the serving size the same?
    if (foodDrinkIsAlreadyAdded) {
      isSameServingSize = this.foodDrinkHasSameServingSize(selectedFoodDrink);
    }

    // ------------------------------------------------------------------
    // Handle if no meal is selected by telling the user to select a meal.
    if (!selectedMeal || !selectedMeal.hasOwnProperty("id")) {
      this.setState({ showSelectAMealMessage: true });
      return;
    } 


    // -------------------------------------------
    // Handle adding a foodDrink to the found meal
    foundMeal = meals.find((meal, index) => {
      if (meal.id === selectedMeal.id) {
        mealIndex = index;
        return meal;
      } else {
        return undefined;
      }
    });

    // Handle if the foodDrink isn't in the meal
    if (!foodDrinkIsAlreadyAdded) {
      foundMeal.foodDrinks.push(selectedFoodDrink);
      mealsToUpdate[mealIndex] = foundMeal;
      foodDrinkToUpdate.servingSize = "";
      foodDrinkToUpdate.quantity = "";

      this.setState(
        {
          meals: mealsToUpdate,
          formSubmitted: false,
          selectedFoodDrink: foodDrinkToUpdate
        },
        () => {
          LocalStorage.addToLocalStorage("meals", this.state.meals); 
        }
      );

    // Handle if the foodDrink is already in the meal
    } else {
      // If it is not the same serving size, add it to the meal. No restrictions.
      if (!isSameServingSize) {
        foundMeal.foodDrinks.push(selectedFoodDrink);
        mealsToUpdate[mealIndex] = foundMeal;

      // Otherwise increment the quantity
      } else {
        foodDrinkIndex = foundMeal.foodDrinks.findIndex(foodDrink => {
          return (
            foodDrink._id === selectedFoodDrink._id &&
            foodDrink.servingSize === selectedFoodDrink.servingSize
          );
        });
        foundMeal.foodDrinks[foodDrinkIndex].quantity += parseFloat(
          quantity
        );
        mealsToUpdate[mealIndex] = foundMeal;
      }

      // foodDrinkToUpdate.servingSize = "";
      // foodDrinkToUpdate.quantity = "";

      this.setState({
          meals: [...mealsToUpdate],
          selectedMeal: foundMeal,
          formSubmitted: false,
          selectedFoodDrink: Object.assign({}, this.state.selectedFoodDrink, foodDrinkToUpdate)
        }, () => {
          console.log('addFoodDrinkToMeal', this.state.selectedFoodDrink);
          LocalStorage.addToLocalStorage("meals", this.state.meals)
      });
    }
  }

  deleteMeal = (mealId) => {
    const { meals } = this.state;
    let filteredMeals;

    if (this.state.meals.length === 1) {
      filteredMeals = [];
    } else {
      filteredMeals = meals.filter(meal => meal.id !== mealId);
    }
    this.setState(
      {
        meals: [...filteredMeals],
        selectedMeal: filteredMeals.length
          ? filteredMeals[filteredMeals.length - 1]
          : {id: "Choose a meal"}
      },
      () => {
        console.log(this.state.meals);
        if (this.state.meals.length === 0) {
          LocalStorage.clearLocalStorage();
        } else {
          LocalStorage.addToLocalStorage("meals", this.state.meals);
        }
      }
    );
  }

  foodDrinkAlreadyExists = (foodDrink) => {
    const { selectedMeal } = this.state;
    let existingFoodDrink;

    if (
      selectedMeal &&
      selectedMeal.hasOwnProperty("foodDrinks") &&
      selectedMeal.foodDrinks.length
    ) {
      existingFoodDrink = selectedMeal.foodDrinks.some((_foodDrink, index) => {
        return _foodDrink._id === foodDrink._id;
      });
    }

    return existingFoodDrink;
  }

  foodDrinkHasSameServingSize = (foodDrink) => {
    const { selectedMeal } = this.state;
    let sameServingSize;

    if (
      selectedMeal &&
      selectedMeal.hasOwnProperty("foodDrinks") &&
      selectedMeal.foodDrinks.length
    ) {
      sameServingSize = selectedMeal.foodDrinks.some((_foodDrink, index) => {
        return _foodDrink.servingSize === foodDrink.servingSize;
      });
    }

    return sameServingSize;
  }

  saveMeals = () => {
    console.log(`[MakeAMeal -> saveMeals()]`);
    saveMeals(this.props.user.username, this.state.meals)
      .then((response) => {
        console.log(`MakeAMeal() response`, response, response.data.data.user);

        if (response.status === 200) {
          console.log(response.data.data);
          this.props.setUser(response.data.data.user);
          LocalStorage.addToLocalStorage('meals', this.state.meals);
          LocalStorage.removeItemFromLocalStorage('selectedMeal');
          this.props.setAlert(true, "Saved meals!");

          setTimeout(() => {this.props.setAlert(false)}, 3000);
        }
        // HTTP request
        // On success LocalStorage.setItem()
      })
      .catch(error => {
        console.log(`An error occured setting a meal`, error);
      });
  }

  setSelectedMeal = (selectedMeal) => {
    this.setState(
      {
        selectedMeal: Object.assign({}, this.state.selectedMeal, selectedMeal)
      },
      () =>
        LocalStorage.addToLocalStorage("selectedMeal", this.state.selectedMeal)
    );
  }

  makeAMealName = (name, id) => {
    const { meals, selectedMeal } = this.state;
    let selectedMealIndex;
    let newMealsState = this.state.meals;

    meals.find((meal, index) => {
      if (meal.id === selectedMeal.id) {
        newMealsState[index]['name'] = name;

        this.setState({
            meals: [newMealsState],
            mealName: ""
        }, () => LocalStorage.addToLocalStorage("meals", this.state.meals));

      } else {
        return undefined;
      }
    });
  }

  addNewMeasurement = (id) => {
    console.log(`[MakeAMeal addNewMeasurement(id)]`, id);
    // find consumable.id.measurements.push(this.state.newMeasurement);
    // update LocalStorage
    // update user object Redux
    // something about if an existing newMeasurement is toggledOpen in another different
    // food item than close it and clear its input
  }

  toggleMealNameInput = (value) => {
    console.log(`[MakeAMeal] toggleMealNameInput()`);
    this.setState({ showMealNameInput: value === "open" ? true : false });
  }

  saveMealName = () => {
    console.log(`[MakeAMeal -> saveMealName()]`);
    const { selectedMeal, mealName, meals } = this.state;

    let foundMeal = meals.find(meal => meal.id === selectedMeal.id);
    foundMeal.name = mealName;

    this.setState(
      {
        meals: meals.map(meal =>
          meal.id === selectedMeal.id ? { ...meal, name: mealName } : meal
        ),
        selectedMeal: Object.assign({}, this.state.selectedMeal, foundMeal)
      },
      () => {
        console.log(this.state.meals);
        LocalStorage.addToLocalStorage('meals', this.state.meals);
      }
    );
  }

  setFormSubmitted = (formSubmitted) => {
    this.setState({ formSubmitted });
  }

  handleFormOnSubmit = () => {
    const { selectedFoodDrink } = this.state;
    const { servingSize, quantity } = selectedFoodDrink;
    const foodFormIsValid = this.isFoodFormValid();
    this.setFormSubmitted(true);

    if (foodFormIsValid) {
      if (servingSize !== "" && quantity && quantity !== "")
        this.addFoodDrinkToMeal();
    }
  }

  handleOnCloseDetail = () => {
    this.setState({ showDetailModal: false });
  }

  isFoodFormValid = () => {
    const { servingSize, quantity } = this.state.selectedFoodDrink;

    return servingSize !== "" && quantity;
  }

  // ------------ OnChange -----------
  handleMealNameOnChange = (name = "") => {
    console.log(`[handleMealNameOnChange] name`, name.trim());
    this.setState({
        mealName: name.trim()
    });
  }

  handleFoodDrinkSelectOnChange = (event) => {
    console.log(
      `[MakeAMeal] handleFoodDrinkSelectOnChange() event`,
      event.target.value
    );

    const { user } = this.props;
    const { foodDrinks } = user;
    const id = event.target.value;

    let selectedFoodDrink;

    if (foodDrinks && foodDrinks.length) {
      selectedFoodDrink = foodDrinks.find(item => item._id === id);

      if (selectedFoodDrink) {
        this.setState({
          formSubmitted: false,
          selectedFoodDrink: {
            ...this.state.selectedFoodDrink,
            ...selectedFoodDrink,
            servingSize: "",
            measurement: selectedFoodDrink.selectedMeasurement,
            quantity: ""
          }
        });
      }
    }
  }

  handleNewMeasurementOnChange = (event) => {
    // something about if another food newMeasurement input is populated
    // than clear it and close the input
    // and this input is not sanitized. should it be sanit ized trim() etc.?
    this.setState({ newMeasurement: event.target.value }, () => {
      console.log(this.state.newMeasurement);
    });
  }

  handleQuantityOnChange = (event) => {
    const { selectedFoodDrink } = this.state;

    this.setState(
      {
        selectedFoodDrink: {
          ...selectedFoodDrink,
          quantity: Number(event.target.value)
        }
      },
      () => console.log(this.state.selectedFoodDrink.quantity)
    );
  }

  handleServingSizeOnChange = (event) => {
    console.log(`[MakeAMeal] handleServingSizeOnChange()`, event.target.value);
    const { selectedFoodDrink } = this.state;

    this.setState(
      {
        selectedFoodDrink: Object.assign({}, this.state.selectedFoodDrink, { servingSize: event.target.value })
      },
      () => console.log(this.state.selectedFoodDrink.servingSize)
    );
  }

  handleMeasurementOnChange = (event) => {
    const { selectedFoodDrink } = this.state;
    this.setState({
      selectedFoodDrink: {
        ...selectedFoodDrink,
        measurement: event.target.value
      }
    });
  }

  handleMealSelectOnChange = (event) => {
    console.log(`[MakeAMeal] handleMealSelectOnChange()`, event.target.value);

    const foundMeal = this.state.meals.find(
      meal => meal.id === parseInt(event.target.value, 10)
    );

    if (foundMeal) {
      this.setState({
        selectedMeal: Object.assign({}, this.state.selectedMeal, foundMeal),
        showSelectAMealMessage: false
      }, () => LocalStorage.addToLocalStorage('selectedMeal', this.state.selectedMeal));
    }
  }
  // ------------ / OnChange --------

  confirmMealReset = () => {
    this.props.beingConfirmMealReset24Hours
  }

  render() {
    const { user } = this.props;
    const {
      meals,
      servingSizes,
      selectedMeal,
      timezoneGuess,
      formSubmitted,
      selectedFoodDrink,
      showSelectAMealMessage
    } = this.state;
    const { servingSize, quantity } = selectedFoodDrink;
    let noFoodDrinksYet = user.foodDrinks && user.foodDrinks.length < 1;
    const foodDrinksSelectClassNames = classNames(
      "flex",
      "align-start",
      { "space-between": !noFoodDrinksYet },
      { "justify-start": noFoodDrinksYet }
    );
    let Meals;
    let AddButton;
    let MealsOptions;
    let NoMealsMessage;
    let AddFirstFoodItem;
    let FoodDrinkOptions;
    let QuantityErrorMessage;
    let SelectedFoodDrinkForm;
    let ShowSelectAMealMessage;
    let ServingSizeErrorMessage;

    //------------------------------------------------------
    // Conditional Components ( TODO convert to components )

    // ServingSizeMeasurement
    if (formSubmitted && (!servingSize || servingSize === "")) {
      ServingSizeErrorMessage = (
        <div className="food-drink-error serving-size-error-message">
          Select a serving size.
        </div>
      );
    }

    // QuantityErrorMessage
    if (formSubmitted && (!quantity || parseFloat(quantity) === 0)) {
      QuantityErrorMessage = (
        <div className="food-drink-error quantity-error-message">
          Enter a quantity
        </div>
      );
    }

    // NoMealsMessage
    if (!selectedMeal && (meals && meals.length > 0)) {
      NoMealsMessage = <div className="no-meals-text">Please select a meal.</div>;
    }
    if (!meals || !meals.length) {
      NoMealsMessage = <div className="no-meals-text">No meals created today.</div>;
    }

    // AddFirstFoodItem
    if (noFoodDrinksYet) {
      AddFirstFoodItem = (
        <div className="add-first-food-item">
          <Link to="/me/add">Add my first item</Link>
        </div>
      );
    }

    if (
      typeof selectedMeal !== "String" && 
      (meals && meals.length)
    ) {
      Meals = (
        <Meal
          selectedMeal={selectedMeal}
          meal={this.state.meals.find(meal => meal.id === selectedMeal.id)}
          mealsLength={meals.length}
          deleteMeal={mealId => this.deleteMeal(mealId)}
          saveMealName={this.saveMealName}
          handleOnChange={name => this.handleMealNameOnChange(name)}
        />
      );
    }

    // FoodDrinkOptions
    // TODO So the way this works is, a user adds a new item with only 1 measurment ie bowl
    // If they want to add the item again with a different measurement than the 2nd
    // item and following items must have paranthesis showing the different measurement type
    // Oatmeal
    // Oatmeal (cup)
    // Oatmeal (tablespoon)
    // How to do this?
    // Add a new property to items, itemAlreadyExists
    // Or add each item
    // has a someAdjectiveDescribingHowItHasManyTypesOfMeasurements: [ { type: 'bowl', vitamins: []}, { type: 'tablespoon', vitamins: []} ]
    // when updating quantity  oatmeal.type === 'bowl'.quantity = quantity;
    if (user.foodDrinks && user.foodDrinks.length) {
      FoodDrinkOptions = user.foodDrinks.sort().map((foodDrink, index) => {
        return (
          <option
            className="food-drink-option"
            name="food-drink-option"
            value={foodDrink._id}
            key={index}
          >
            {foodDrink.name} [{foodDrink.foodOrDrink}] ({foodDrink.selectedMeasurement})
          </option>
        );
      });
    }

    // SelectedFoodDrinkForm
    if (selectedFoodDrink.hasOwnProperty("_id")) {
      SelectedFoodDrinkForm = (
        <div
          ref="food-drink"
          data-food-drink-id={selectedFoodDrink._id}
          className={`flex center-all ${
            selectedFoodDrink.typeOfItem === "food" ? "food" : "drink"
          }`}
        >
          <form className="food-drink--form flex space-around align-center">
            <div className="serving-size">
              <label htmlFor="serving-size-select">
                Serving Size
                <select
                  ref="serving-size"
                  name="serving-size"
                  id="serving-size-select"
                  value={selectedFoodDrink.servingSize}
                  onChange={this.handleServingSizeOnChange}
                >
                  <option disabled selected value="">
                    Select a Serving
                  </option>
                  {servingSizes.map((servingSize, index) => {
                    return (
                      <option key={index + 1} value={servingSize}>
                        {servingSize}
                      </option>
                    );
                  })}
                </select>
                {ServingSizeErrorMessage}
              </label>
            </div>

            <div className="quantity">
              <label htmlFor="quantity-input">
                Quantity
                <input
                  id="quantity-input"
                  type="number"
                  name="quantity"
                  value={String(quantity)}
                  placeholder="How many?"
                  onChange={event => {
                    this.setFormSubmitted(false);
                    this.handleQuantityOnChange(event);
                  }}
                />
                {QuantityErrorMessage}
              </label>
            </div>
            {/*
                <div className="buttons">
                  <i className="fa fa-info"></i>
                  <button type="button">Calculate</button>
                  <button type="button">Smart Calculate</button>
                </div>
                */}
          </form>
        </div>
      );
    }

    // MealsOptions
    if (meals && meals.length) {
      MealsOptions = this.state.meals.map((meal, index) => {
        return (
          <option key={index + 1} value={meal.id}>
            {meal.name}
          </option>
        );
      });
    }

    // AddButton
    if (selectedFoodDrink.hasOwnProperty("_id")) {
      AddButton = (
        <button type="button" className="add" onClick={this.handleFormOnSubmit}>
          Add
        </button>
      );
    }

    if (showSelectAMealMessage && formSubmitted) {
      ShowSelectAMealMessage = (
        <div className="select-a-meal-message flex center-all">
          Please select a meal
        </div>
      );
    }

    // Conditional Components
    //-------------------------------------------

    if (this.props.user.isAuthenticated) {
      return (
        <div className="make-a-meal view center-all" styles={styles}>
          {/* Header */}
          <div className="view--header flex align-center space-between">
            <div className="flex space-around align-center">
              <h1 className="view--title">{this.state.title}</h1>
              <h2 className="date-time">
                <span className="date">{this.state.date}</span> &nbsp;&nbsp;
                <span className="time">
                  <Clock
                    format={"h:mm A"}
                    ticking={true}
                    timezone={timezoneGuess}
                  />
                </span>
              </h2>
            </div>
            <div className="buttons flex space-around align-center">
              <button type="button">Save Meals</button>
              <button type="button">
                <Link to="/me/totals">View Totals</Link>
              </button>
            </div>
          </div>

          {/* Main Content */}
          <div className="view--main">
            
            {!this.props.confirm24HourMealResetMessage && 
             <h2 id="meals-reset-message" className="flex space-between">Meals are reset every 24 hours, starting at 12:00am and resetting at 12:00pm <span onClick={this.props.beginConfirmingMealReset} className="lnr lnr-cross"></span></h2>}
  
            {/* Food Drinks - Select Menu on change shows the FoodDrink options and Add To meal */}
            <section
              value={this.state.foodDrinkSelectValue}
              className="food-drinks"
            >
              <h1 className="section-title">Add to a meal</h1>
              <h2 className="food-drinks--add-link">
                <Link to="/me/add">Add a new item</Link>
              </h2>
              <div className="food-drink flex column space-between align-center">
                <div className={foodDrinksSelectClassNames}>
                  <select
                    id="food-drinks--select"
                    className={`${noFoodDrinksYet ? "no-margin-right" : ""}`}
                    name="food-drink-select"
                    defaultValue="Choose"
                    onChange={this.handleFoodDrinkSelectOnChange}
                  >
                    <option disabled>
                      Choose
                    </option>
                    {FoodDrinkOptions}
                  </select>
                  {ShowSelectAMealMessage}
                  {AddFirstFoodItem}
                  {AddButton}
                </div>
                {SelectedFoodDrinkForm}
              </div>
            </section>

            {/* Meals */}
            <section className="meals">
              <h1 className="section-title">My Meals</h1>
              <div className="meals-select-buttons-container flex space-between align-center">
                <div className="meals-select-container">
                  <select
                    ref="meals-select"
                    id="meals-select"
                    defaultValue={typeof this.state.selectedMeal === "string" ? "Choose a meal" : this.state.selectedMeal.id}
                    onChange={this.handleMealSelectOnChange}
                  >
                    <option disabled value="Choose a meal">
                      Choose a meal
                    </option>
                    {MealsOptions}
                  </select>
                </div>
                <div className="buttons flex space-around align-center">
                  <button
                    type="button"
                    className="new-meal"
                    onClick={() => this.createNewMeal()}
                  >
                    New meal
                  </button>
                  <button
                    type="button"
                    className="save-meals"
                    onClick={() => this.saveMeals()}
                  >
                    Save my meals
                  </button>
                </div>
              </div>
              <div className="meals-container">{Meals}</div>
              {NoMealsMessage}
            </section>
          </div>

          {/* FoodDrink Detail 'Modal' */}
          {this.state.showDetailModal && (
            <Detail
              styles={DetailStyles}
              servingSize={this.state.servingSize}
              item={this.state.selectedFoodDrink}
              close={this.handleOnCloseDetail}
            />
          )}
        </div>
      );
    } else {
      return(<div>Waiting...</div>);
    }
  }
}

const mapStateToProps = state => {
  return {
    confirm24HourMealResetMessage: state.userReducers.confirm24HourMealResetMessage,
    showAlert: state.alertReducers.showAlert,
    user: state.userReducers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(setUser(user)),
    setAlert: (isVisible, title, body) => dispatch(setAlert(isVisible, title, body)),
    beginConfirmingMealReset: () => dispatch(confirm24HourMealResetMessage())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MakeAMeal);
