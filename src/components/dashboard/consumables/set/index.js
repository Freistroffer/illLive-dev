import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import * as vitaminsJSON from '../../../../vitamins.json';
import * as styles from './styles.css';
import { setText, toggleAlert } from '../../../alert/alert_actions';
import { setUser } from '../../../user/user_actions';
import configs from '../../../../configs';

export class Add extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: 'Add',
      measurements: []
    }

 
    this.saveItem = this.saveItem.bind(this);
    this.clearForm = this.clearForm.bind(this);
    this.areYouSure = this.areYouSure.bind(this);
    this.handleAnswer = this.handleAnswer.bind(this);
    this.handleAnswer = this.handleAnswer.bind(this);
    this.setTypeOfItem = this.setTypeOfItem.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.turnEditModeOn = this.turnEditModeOn.bind(this); 
    this.deleteAdjective = this.deleteAdjective.bind(this); 
    this.addNewAdjective = this.addNewAdjective.bind(this);
    this.turnEditModeOff = this.turnEditModeOff.bind(this); 
    this.setSelectedMeasurement = this.setSelectedMeasurement.bind(this);
  }



  componentDidMount() {
    const { user } = this.props;
    if ( user && user.measurements ) {
      this.setState({ measurements: user.measurements });
    }
  }



  componentWillReceiveProps() {
    const { user } = this.props; 
    if ( user && user.measurements ) {
      this.setState({ measurements: user.measurements });
    }
  }



  handleOnChange(e) {

    let inputValue = e.target.value;
    let measurementAlreadyExists = false;
    const { user } = this.props; 

    // TODO clean up
    if ( inputValue && e.target.id === 'new-measurement-input' ) {
      
      if ( user && user.hasOwnProperty('adjectives') && Array.isArray(user.adjectives)) {
        measurementAlreadyExists = this.adjectiveAlreadyExists(inputValue);

        if ( !measurementAlreadyExists ) {
          this.setState({ newMeasurement: e.target.value });
        }
      } 
    } else {
      this.setState({ [e.target.name]: e.target.value }); 
    }
  }



  outputQuantity() {
    let quantity = [];

    this.state.quantity.forEach( (q, index) => {
      quantity.push(index + 1); 
    });

    // for(let i = 0; i < this.state.quantity; i++ ) {
    //   quantity.push(i + 1);
    // }

    return quantity.map((quantity, index) => {
      return <option name={`quantity-${quantity }`}>{quantity}</option>
    })
  } 



  outputVitamins() {
    console.log(`[Add] outputVitamins()`);
    return vitaminsJSON.vitamins.map(( vitamin, index ) => {
      return(
        <div className="vitamin" key={index}>
          <label htmlFor={vitamin.id}>
              <span>{vitamin.name}</span> <span>{vitamin.measurement}</span>
          </label>
          <input type="number" name={vitamin.inputName} 
                 value={this.state[vitamin.name]}
                 className="add-form-input"
                 onChange={this.handleOnChange} id={vitamin.id}    
                 placeholder={`Measurement: ${vitamin.measurement}`}/>
        </div>
      )
    });
  }



  outputAdjectives() {
    if ( this.props.user && this.props.user.adjectives ) {
      return this.props.user.adjectives.map( (adjective, index) => {
        return (
          <button type="button" className={ `adjective-button ${ this.state.selectedAdjective === adjective ? 'selected-button' : '' }` } 
                  key={index} styles={styles}
                  onClick={ () => { this.setSelectedAdjective(adjective) } }>
            {adjective}
            { this.state.adjectiveEditMode ?  
              <DeleteAdjective index={index} deleteAdjective={() => {this.deleteAdjective(index) }}></DeleteAdjective> : ''
            }
          </button> 
        )
      });
    } 
  } 



  outputMeasurements() {
    // TODO should be this.props.measurements
    // TODO ensure it's an array and .length is not undefined.
    if ( this.state.measurements.length > 0 ) {
      return this.state.measurements.map( (measurements, index) => {
        return (
          <button type="button" className={ `measurements-button ${ this.state.selectedMeasurement === measurements ? 'selected-button' : '' }` } 
                  key={index} styles={styles}
                  onClick={ () => { this.setSelectedMeasurement(measurements) } }>
            {measurements}
          </button> 
        )
      });
    }
  }



  areYouSure() {
    this.setState({ showAreYouSure: true }, () => {
      console.log(`showAreYouSure is true`);
      // TOOD show the areYouSure modal
    });
  }



  handleAnswer(answer) {
    console.log(`[Add] handleAnswer( ${answer} )`);
    this.setState({ showAreYouSure: false }, () => {
      if ( answer === 'yes' ) {
        this.clearForm();
        // // TODO delete session using sessionStorage action creater
      }
    });
  }



  clearForm() {
    console.log(`[Add] clearForm()`);

    this.setState({
      itemName: '',
      typeOfItem: '',
      selectedAdjective: '',
      selectedMeasurement: '',
      newAdjective: '',
      adjectives: ['Teaspoon', 'Tablespoon', 'Cup', 'Quart', 'Per', 'Slice', 'Handful', 'Leaf'],
      vitaminA: '',
      vitaminB1: '',
      vitaminB2: '',
      vitaminB3: '',
      vitaminB5: '',
      vitaminB6: '',
      vitaminB7: '',
      vitaminB12: '',
      calcium: '',
      choline: '',
      chromium: '',
      copper: '',
      flouride: '',
      folicAcid: '',
      iodinee: '',
      iron: '',
      magnesium: '',
      molybdenum: '',
      phosphorus: '',
      potassium: '',
      selenium: '',
      salt: '',
      vitaminD3: '',
      vitaminE: '',
      vitaminK: '',
      zinc: '',
      formSubmitted: false,
      selectedTypeOfRequiredMessage: 'Selecting food or drink is required',
      selectedAdjectiveRequiredMessage: 'Selecting a description is required',
      selectedMeasurementRequiredMessage: 'Selecting a measurement is required'
    }, () => {
      // Clear item name, description and vitamin values
      document.getElementById('add-name').value = '';
      document.querySelectorAll('.add-form-input').forEach( (input) => {
        input.value = "";
      });
    });

    this.props.setAlertText('Form cleared!');
    this.props.toggleAlert(true);
  }



  setTypeOfItem( type ) {
    console.log(`[Add] setTypeOfItem() ${type}`);
    this.setState({
      typeOfItem: type 
    }, () => {
      // TODO update session using sessionStorage action creater
    });
  }



  setSelectedAdjective( selectedAdjective ) {

    this.turnEditModeOff();

    if ( this.state.selectedAdjective === selectedAdjective ) {
      this.setState({
        selectedAdjective: ''
      });
    } else {
      console.log(`[Add] setSelectedAdjective() ${selectedAdjective}`);
      this.setState({
        selectedAdjective 
      }, () => {
        // TODO update session using sessionStorage action creater
      });
    }
  }

  

  addNewAdjective() {
    const { newAdjective } = this.state
    const { adjectives } = this.props.user;

    console.log(`[Add] addNewAdjective()`, newAdjective);
    
    if ( newAdjective && newAdjective !== '' ) {
      if ( adjectives && adjectives.length ) {
        this.props.addNewAdjective( newAdjective );

        this.setState({
          newAdjective: ''
        });
      }
    }
  }


  turnEditModeOn() {
    this.setState({ adjectiveEditMode: true });
  }

  turnEditModeOff() {
    this.setState({ adjectiveEditMode: false });
  }



  deleteAdjective(index) {
    const adjective = this.props.user.adjectives[index];
    deleteAdjective(adjective, index);
  }



  adjectiveAlreadyExists(newAdjective) {
    return this.props.user.adjectives.some((adjective, index) => {
      return adjective.toLowerCase() == newAdjective.toLowerCase();
    });
  }


  // TODO
  // Check if all the vitamin inputs are empty or say if only 5 are completed,
  // Output a modal saying most of the vitamin inputs aren't filled in, are you sure
  // you want to add the item?
  async formIsValid() {
    console.log(`[Add] formIsValid()`);
    const { 
      typeOfItem, 
      selectedAdjective, 
      selectedMeasurement 
    } = this.state; 

    console.log(
      typeOfItem, 
      selectedAdjective, 
      selectedMeasurement 
    );

    console.log(typeOfItem !== '' && selectedAdjective !== '');
    return (typeOfItem && selectedAdjective ) !== '';
  }



  saveItem() {
    console.log(`[Add] saveItem()`)
    
    // Form is submitted ...
    this.setState({ formSubmitted: true }, () => {

      const { 
        typeOfItem, 
        selectedAdjective, 
        selectedMeasurement,
        formSubmitted 
      } = this.state; 

      // TODO add a feature asking the user if they want to save this item for later, 
      // or "I just bought something random at the store and am inputting the 3 vitamins
      //     that are on the label real quick and probably won't eat this for a long time."
      // SAVE FOR LATER CHECKBOX
      // Also save the values in sessionStorage
      
      // Is the form valid?
      this.formIsValid().then( validForm => {

        if (validForm) {
          console.log(`formIsValid`)

          const item = {
            details: {
              itemName: this.state.itemName,
              typeOfItem: this.state.typeOfItem,
              adjectives: [...this.state.adjectives],
              selectedAdjective: this.state.selectedAdjective
            },
            vitamin: {
              vitaminA: this.state.vitaminA,
              vitaminB1: this.state.vitaminB1,
              vitaminB2: this.state.vitaminB2,
              vitaminB3: this.state.vitaminB3,
              vitaminB5: this.state.vitaminB5,
              vitaminB6: this.state.vitaminB6,
              vitaminB7: this.state.vitaminB7,
              vitaminB12: this.state.vitaminB12,
              calcium: this.state.calcium,
              choline: this.state.choline,
              chromium: this.state.chromium,
              copper: this.state.copper,
              flouride: this.state.flouride,
              folicAcid: this.state.folicAcid,
              iodine: this.state.iodine,
              iron: this.state.iron,
              magnesium: this.state.magnesium,
              molybdenum: this.state.molybdenum,
              phosphorus: this.state.phosphorus,
              potassium: this.state.potassium,
              selenium: this.state.selenium,
              salt: this.state.salt,
              vitaminD3: this.state.vitaminD3,
              vitaminE: this.state.vitaminE,
              vitaminK: this.state.vitaminK,
              zinc: this.state.zinc
            }
          };

          console.log(`calling saveItem`, item);
          saveItem(item)
          .then(response => {
            console.log(`response`, response.data);
            
            this.props.setAlertText("Item added!");
            this.props.toggleAlert(true);

            this.clearForm();

            this.setState({
              adjectives: response.data.data.payload.user.adjectives 
            });

            this.props.setUser( response.data.data.payload.user );

            setTimeout(() => {
              this.props.setAlertText('');
              this.props.toggleAlert(false);
            }, 2800);
          })
          .catch(error => {
            console.log(`[Add -> saveItem] An error occured`, error)
          });
        }
      });
    });

  }



  render() { 
    
    const { user } = this.props;

    let name = (user && user.email) ? user.email : ''
    let adjectives;
  

    console.log(`render user.adjectives`, user.adjectives);
   
    if ( user.adjectives && user.adjectives.length ) {
      adjectives = user.adjectives.map( (adjective, index) => {
        return (
          <button type="button" className={ `adjective-button ${ this.state.selectedAdjective === adjective ? 'selected-button' : '' }` } 
                  key={index} styles={styles}
                  onClick={ () => { this.setSelectedAdjective(adjective) } }>
            {adjective}
            { this.state.adjectiveEditMode ?  
              <DeleteAdjective index={index} deleteAdjective={() => {this.deleteAdjective(index) }}></DeleteAdjective> : ''
            }
          </button> 
        )
      });
    }
  

    let FormErrors;
    const { typeOfItem, selectedAdjective, selectedMeasurement, formSubmitted } = this.state;
    
    {/* Adjective components */}

      let EditAdjectiveButton = '';

      if ( this.state.adjectives && this.state.adjectives.length ) {
        EditAdjectiveButton = (<button type="button" className="adjective-edit-button" onClick={() => { !this.state.adjectiveEditMode ? this.turnEditModeOn() : this.turnEditModeOff()  }}>Delete</button>);
      }
    {/* / Adjective components */}

    return (
      <div className="add view center-all" styles={styles}>
        <h1 className="view--title">{this.state.title}</h1>
        <form>

          {/* Type of item */}
          <div className="form-group center-all">
            <h2 className="form-group--title">Is this a food or a drink item?</h2>
            <div className="form-group--content type-of-item-buttons">
              <button type="button" 
                      className={ `type-of-item-button ${this.state.typeOfItem === 'food' ? 'selected-button'  : '' }`  } 
                      onClick={ () => this.setTypeOfItem('food') }>Food</button>
              <button type="button" 
                      className={ `type-of-item-button ${this.state.typeOfItem === 'drink' ? 'selected-button' : '' }`  } 
                      onClick={ () => this.setTypeOfItem('drink') }>Drink</button>
            </div>
          </div>
          


          {/* Item Name */}
          <div className="form-group">
            <h2 className="form-group--title">Item Name</h2>
            <div className="form-group--content">
              <input type="text" name="itemName" id="add-name" placeholder="Item name" onChange={this.handleOnChange} />
            </div>
          </div>



          {/* Adjectives */}
          <div className="form-group adjectives-group">
            <h2 className="form-group--title">Measurement Description</h2>
            <div className="form-group--content">
              <div className="adjectives">
                {adjectives}
              </div> 
              <label htmlFor="new-adjective-input">
              Need another description?
              <input type="text" name="new-adjective" 
                     value={this.state.newAdjective}
                     onChange={this.handleOnChange}
                     id="new-adjective-input" placeholder="Adjective"  />
              </label>
              <button className="adjective-add-button" type="button" onClick={this.addNewAdjective}>Add</button>
              {EditAdjectiveButton}
            </div>
          </div>



          {/* Vitamins */}
          <div className="form-group vitamins-group">
            <h2 className="form-group--title">Vitamins</h2>
            <div className="form-group--content">{this.outputVitamins()}</div>
          </div>
          


          {/* Form Controls */}
          <div className="form-group form-group-form-controls  buttons center-all">
            <button className="btn btn-primary button-add" type="button" onClick={this.saveItem}>Add</button>
            <button className="btn btn-primary button-clear" type="button" onClick={this.areYouSure} >Clear</button>
            {FormErrors}
          </div>
        </form>


        
        {/* "Are you sure?" modal */}
        {
          this.state.showAreYouSure ? <div style={confirmContainerStyles}><AreYouSure answerIs={ this.handleAnswer } /></div> : null
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    showAlert: state.alertReducers.showAlert,
    user: state.userReducers
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setAlertText: ( title, body ) => { dispatch( setText( title, body ) ) },
    toggleAlert: ( showAlert ) => { dispatch( toggleAlert( showAlert ) ) },
    setUser: user => { dispatch(setUser(user)) },
    addNewAdjective: adjective => dispatch(addNewAdjective(adjective))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Add);
