import React from 'react';
import { Input } from 'antd';

export const FoodDrinkInputName = (props) => {
  return(
    <div className="stepper stepper--name">
      <h2 className="stepper--title">What is the name of this?</h2>
      <div className="stepper--content">
        <Input
          type="text"
          name="foodDrinkInputName"
          id="food-drink-name-input"
          placeholder="Food or drink name"
          value={props.name}
          allowClear={true}
          onChange={props.handleOnChange}
        />
      </div>
    </div>    
  );
}