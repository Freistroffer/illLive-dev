import React, { useEffect } from 'react';
import { Input } from 'antd';

export const Vitamins = (props) => {

	useEffect(() => {
		console.log(props.state);
	});
	return(
		<div className="stepper stepper--vitamins">
			<h2 className="stepper--title">Vitamins</h2>
			<div className="stepper--content">
				{
				    props.vitaminsJSON.vitamins.map((vitamin, index) => {
					  	return (
					        <div className="vitamin" key={index}>
					          <label htmlFor={vitamin.id}>
					            <span>{vitamin.name}</span> <span>({vitamin.measurement})</span>
					          </label>
					          <Input  
					            type="number"
					            name={vitamin.inputName}
					            className="add-form-input"
					            onChange={(e) => {
					            	console.log(e.target.value);
					            	props.handleOnChange(e);
					           	}}
					            id={vitamin.id}
					            placeholder={`${vitamin.measurement}`}
					          />
					        </div>
					    );
	    			})
				}
			</div>
		</div>
	);
}