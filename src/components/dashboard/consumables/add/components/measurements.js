import React, {useEffect} from 'react';
import { Select } from 'antd';

const Option = Select.Option;

export const Measurements = (props) => {
	let MeasurementOptions;

	useEffect(() => {
		console.log(props.measurements)
		MeasurementOptions = props.measurements.map(
			(measurement, index) => {
				return (
					<option
						key={index + 1}
						name="measurement-option"
						value={measurement.toString()}
					>
					{measurement}
					</option>
				);
			}
		);
	});

	return(
		<div className="stepper stepper--measurements">
			<h2 className="stepper--title">Measurement Description</h2>
				{props.oneMeasurementRequiredMessage}
			<div className="stepper--content flex row justify-start align-center wrap">
				<Select
					id="measurements-select"
					defaultValue="Select a Measurement"
					onChange={props.handleOnChange}
					showSearch
					style={{ width: 200 }}
					placeholder="Select a person"
					optionFilterProp="children"
					filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				>
				  {props.measurements.map(
						(measurement, index) => {
							return (
								<Option
									key={index + 1}
									name="measurement-option"
									value={measurement.toString()}
								>
								{measurement}
								</Option>
							);
						}
					)}
				</Select>
			</div>
		</div>
	);
} 

export default Measurements;