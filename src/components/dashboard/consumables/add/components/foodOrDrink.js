import React from "react";
import { Select } from 'antd';

const Option = Select.Option;

export const FoodOrDrink = props => {
  return (
    <div className="stepper stepper--type">
      <h2 className="stepper--title">Is this a food or a drink item?</h2>
      <div className="stepper--content">
        <Select
          id="measurements-select"
          defaultValue="Select a type"
          onChange={props.handleOnChange}
          style={{ width: 200 }}
          placeholder="Select a type"
        >
          <Option key={2} value="food">
            Food
          </Option>
          <Option key={3} value="drink">
           Drink
          </Option>
        </Select>
      </div>
    </div>
  );
};
