import React, { Component } from "react";
import { Button } from 'antd';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import * as vitaminsJSON from "../../../../vitamins.json";
import * as styles from "./styles.css";
import {
  AreYouSure,
  FoodDrinkInputName,
  FoodOrDrink, 
  Measurements,
  Vitamins
} from "./components";
import { setAlert } from "../../../alert/alert_actions";
import { setUser } from "../../../user/user_actions";
import {
  beginSavingNewFoodDrink,
  pushToSaveFoodDrinkErrors
} from "../../dashboard_actions";
import {
  MEASUREMENT_REQUIRED,
  FOOD_OR_DRINK_REQUIRED,
  FOODDRINK_NAME_REQUIRED
} from "../../types";

const confirmContainerStyles = {
  position: "fixed",
  top: "0",
  right: "0",
  bottom: "0",
  left: "0",
  width: "100%",
  height: "100vh",
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "rgba(2, 2, 2, 0.5)"
};

export class Add extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 1,
      steps: [
        {
          name: "name",
          isValid: false
        },
        {
          name: "type",
          isValid: false
        },
        {
          name: "measurements",
          isValid: false
        },
        {
          name: "vitamins"
        }
      ],
      title: "Save a new consumable",
      foodDrinkInputName: "",
      foodOrDrink: "",
      selectedMeasurement: "Select a Measurement",
      measurements: [],
      servingSize: '',
      showAreYouSure: false,
      measurementEditMode: false,
      vitaminA: 0,
      vitaminB1: 0,
      vitaminB2: 0,
      vitaminB3: 0,
      vitaminB5: 0,
      vitaminB6: 0,
      vitaminB7: 0,
      vitaminB12: 0,
      calcium: 0,
      choline: 0,
      chromium: 0,
      copper: 0,
      fluoride: 0,
      folicAcid: 0,
      iodinee: 0,
      iron: 0,
      magnesium: 0,
      molybdenum: 0,
      phosphorus: 0,
      potassium: 0,
      selenium: 0,
      salt: 0,
      vitaminD3: 0,
      vitaminE: 0,
      vitaminK: 0,
      zinc: 0,
      formSubmitted: false,
      showOneMeasurementRequiredMessage: false,
      selectedTypeOfRequiredMessage: "Please select a food or drink item.",
      selectedMeasurementRequiredMessage:
        "Selecting a measurement is required.",
      atLeastOneMeasurementRequiredMessage:
        "You must keep at least once measurement.",
      formErrors: []
    };

    document.title = "New Food or Drink";
  }

  componentDidUpdate(prevProps, prevState) {

    // Handle success saving a new foodDrink.
    if (!prevProps.successSavingNewFoodDrink && this.props.successSavingNewFoodDrink) {
      this.clearForm();
    }

    //Handle if the saveNewFoodDrinkErrors array obtains a length
    if (
      !prevProps.saveNewFoodDrinkErrors.length &&
      this.props.saveNewFoodDrinkErrors.length > 0
    ) {
      setAlert(true, `Please fix these errors`, `fdsafds`);
    }
  }

  // Rename this to something about handling vitamin inputs on change
  handleOnChange = (event) => {
    console.log(event.target.name, event.target.value);
    this.setState({[event.target.name]: event.target.value });
  };

  handleFoodDrinkInputOnChange = event => {
    let stepsToUpdate = this.state.steps.slice();

    stepsToUpdate[0].isValid = event.target.value.trim() === "" ? false : true;

    this.setState({
      foodDrinkInputName: event.target.value.trim(),
      steps: stepsToUpdate
    });
  };

  handleNewMeasurementOnChange = event => {
    const measurementIsAlreadyAdded = this.measurementIsAlreadyAdded(
      event.target.value
    );

    if (!measurementIsAlreadyAdded) {
      this.setState({ newMeasurement: event.target.value });
    }
  };

  outputQuantity = () => {
    let quantity = [];

    this.state.quantity.forEach((q, index) => {
      quantity.push(index + 1);
    });

    return quantity.map((quantity, index) => {
      return <option name={`quantity-${quantity}`}>{quantity}</option>;
    });
  };

  outputVitamins = () => {

  };

  areYouSure = () => {
    this.setState({ showAreYouSure: true });
  };

  handleAnswer = answer => {
    this.setState({ showAreYouSure: false }, () => {
      if (answer === "yes") {
        this.clearForm();
        // // TODO delete session using sessionStorage action creater
      }
    });
  };

  clearForm = () => {
    this.setState({
        step: 1,
        steps: [
          {
            name: "name",
            isValid: false
          },
          {
            name: "type",
            isValid: false
          },
          {
            name: "measurements",
            isValid: false
          },
          {
            name: "vitamins"
          }
        ],
        title: "Save a new consumable",
        foodDrinkInputName: "",
        foodOrDrink: "",
        selectedMeasurement: "Select a Measurement",
        measurements: [],
        servingSize: '',
        showAreYouSure: false,
        measurementEditMode: false,
        vitaminA: 0,
        vitaminB1: 0,
        vitaminB2: 0,
        vitaminB3: 0,
        vitaminB5: 0,
        vitaminB6: 0,
        vitaminB7: 0,
        vitaminB12: 0,
        calcium: 0,
        choline: 0,
        chromium: 0,
        copper: 0,
        fluoride: 0,
        folicAcid: 0,
        iodinee: 0,
        iron: 0,
        magnesium: 0,
        molybdenum: 0,
        phosphorus: 0,
        potassium: 0,
        selenium: 0,
        salt: 0,
        vitaminD3: 0,
        vitaminE: 0,
        vitaminK: 0,
        zinc: 0,
        formSubmitted: false,
        showOneMeasurementRequiredMessage: false,
        selectedTypeOfRequiredMessage: "Please select a food or drink item.",
        selectedMeasurementRequiredMessage:
          "Selecting a measurement is required.",
        atLeastOneMeasurementRequiredMessage:
          "You must keep at least once measurement.",
        formErrors: []
      },
      () => {
        // Clear item name, description and vitamin values
        document.getElementById("food-drink-name-input").value = "";
        document.querySelectorAll(".add-form-input").forEach(input => {
          input.value = "";
        });
      }
    );
  };

  setfoodOrDrink = type => {
    this.setState(
      {
        foodOrDrink: type
      },
      () => {
        // TODO update session using sessionStorage action creater
      }
    );
  };

  setSelectedMeasurement = selectedMeasurement => {
    this.turnEditModeOff();
    this.setState({ selectedMeasurement });
  };

  addNewMeasurement = () => {
    const { newMeasurement, measurements } = this.state;
    const measurementIsAlreadyAdded = this.measurementIsAlreadyAdded(
      newMeasurement
    );

    console.log(newMeasurement);

    if (!measurementIsAlreadyAdded) {
      this.setState({
        measurements: [...measurements, newMeasurement],
        showOneMeasurementRequiredMessage: false
      });
    }
  };

  turnEditModeOn = () => {
    this.setState({ measurementEditMode: true });
  };

  turnEditModeOff = () => {
    this.setState({ measurementEditMode: false });
  };

  deleteMeasurement = measurementIndex => {
    const { measurements } = this.state;

    // This is the last measurement. You cannot delete it.
    if (measurements.length && measurements.length === 1) {
      this.setState({
        measurementEditMode: false,
        showOneMeasurementRequiredMessage: true
      });
    } else {
      this.setState({
        measurements: this.state.measurements.filter((measurement, index) => {
          return index !== measurementIndex;
        }),
        measurementEditMode: true,
        showOneMeasurementRequiredMessage: false
      });
    }
  };

  measurementIsAlreadyAdded = newMeasurement => {
    return this.state.measurements.some((measurement, index) => {
      return measurement.toLowerCase() === newMeasurement.toLowerCase();
    });
  };

  isValidForm = () => {
    const { foodDrinkInputName, foodOrDrink, selectedMeasurement } = this.state;
    let isValidForm = false;
    let errors = [];

    if (
      foodDrinkInputName !== "" &&
      foodOrDrink !== "" &&
      selectedMeasurement !== ""
    ) {
      isValidForm = true;

      // this.props.notSureSomehowClearSaveFoodDrinkErrors array
      // this.props.clearSaveFoodDrinksErrors()
    } else {
      if (foodDrinkInputName === "") {
        errors.push({
          type: FOODDRINK_NAME_REQUIRED,
          message: "What is the name of your food or drink item?"
        });
      }

      if (foodOrDrink === "") {
        errors.push({
          type: FOOD_OR_DRINK_REQUIRED,
          message: "Is this a food or drink?"
        });
      }

      if (selectedMeasurement === "") {
        errors.push({
          type: MEASUREMENT_REQUIRED,
          message: "Please select a measurement?"
        });
      }

      this.props.pushToSaveFoodDrinkErrors(errors);

      isValidForm = false;
    }

    return isValidForm;
  };

  saveFoodOrDrink = () => {
    const { selectedMeasurement } = this.state;
    // So if the 3 steps are valid, Add.
    // I click on Add, disabled if any of those or formSubmitted.
    // I click on Add, form is submitted, disdabled.
    // Form is not valid, disabled = false
    // otherwise disabled is true
    // beginSavingNewFoodDrink
    // If the form is valid, formSubmitted remains true
    // 
    if (this.isValidForm()) {
        this.setState({ formSubmitted: true }, () => {
          const foodDrink = {
            details: {
              name: this.state.foodDrinkInputName,
              foodOrDrink: this.state.foodOrDrink,
              measurements: [...this.props.user.measurements],
              selectedMeasurement
            },
            vitamin: {
              quantity: this.state.quantity,
              vitaminA: this.state.vitaminA,
              vitaminB1: this.state.vitaminB1,
              vitaminB2: this.state.vitaminB2,
              vitaminB3: this.state.vitaminB3,
              vitaminB5: this.state.vitaminB5,
              vitaminB6: this.state.vitaminB6,
              vitaminB7: this.state.vitaminB7,
              vitaminB12: this.state.vitaminB12,
              calcium: this.state.calcium,
              choline: this.state.choline,
              chromium: this.state.chromium,
              copper: this.state.copper,
              fluoride: this.state.fluoride,
              folicAcid: this.state.folicAcid,
              iodine: this.state.iodine,
              iron: this.state.iron,
              magnesium: this.state.magnesium,
              molybdenum: this.state.selenium,
              salt: this.state.molybdenum,
              phosphorus: this.state.phosphorus,
              potassium: this.state.potassium,
              selenium: this.state.salt,
              vitaminD3: this.state.vitaminD3,
              vitaminE: this.state.vitaminE,
              vitaminK: this.state.vitaminK,
              zinc: this.state.zinc
            }
          };
          this.props.beginSavingNewFoodDrink(foodDrink);
        })
    } else {
      // state.formSubmitted is false by default
    }
  };

  handleMesurementsOnChange = event => {
    this.setState({ newMeasurement: event.target.value.trim() });
  };

  handleFoodOrDrinkOnChange = (foodOrDrink) => {
    let stepsToUpdate = this.state.steps.slice();
    stepsToUpdate[1].isValid = true;
    this.setState({
      steps: stepsToUpdate,
      foodOrDrink
    });
  };

  handleMeasurementSelectOnChange = (measurement) => {
    console.log(`handleMeasurementSelectOnChange`, measurement);
    // fucking stupid. 
    // os yeah i fucking add a fuckign stupid fucking apple as an item or
    // a fucking stupid fucking whatever some fucking non attcahed for real
    // some fucking stupid fucking item that i can measure 
    // with differnet types
    // so theres cup, teaspoon, per, slice etc.
    // so then this new item, i fucking hate how i have to fucking delete 
    // and remove for ewvery item though it is what it is.
    // so each item will have by default those measurements?

    // ideally 
    let stepsToUpdate = this.state.steps.slice();
    stepsToUpdate[2].isValid = true;

    // So yeah fat arrow functions remove the scope restriction??
    this.setState({
      steps: stepsToUpdate,
      selectedMeasurement: measurement
    }, () => console.log(this.state.selectedMeasurement));
  };

  previous = () => {
    const { step, steps } = this.state;

    if (step > 1) {
      this.setState(
        {
          step: step - 1
        },  
        () => {
          console.log(steps, steps[this.state.step]);
        }
      );
    }
  };

  next = () => {
    const { step, steps } = this.state;

    console.log(step);

    if (step  < steps.length) {
      this.setState(
        {
          step: step + 1
        },
        () => {
          console.log(steps, steps[this.state.step]);
        }
      );
    }
  };

  setStep = selectedStep => {
    const { steps } = this.state;
    let stepIndex = steps.findIndex(step => step.name === selectedStep);
    this.setState({ step: stepIndex + 1 });
  };

  render() {
    const {
      step,
      steps,
      showOneMeasurementRequiredMessage,
      atLeastOneMeasurementRequiredMessage
    } = this.state;

    let FormErrors;
    let EditMeasurementButton;
    let OneMeasurementRequiredMessage;

    if (this.props.isAuthenticated) {
      if (showOneMeasurementRequiredMessage) {
        OneMeasurementRequiredMessage = (
          <div className="one-measurement-required">
            {atLeastOneMeasurementRequiredMessage}
          </div>
        );
      }

      if (
        this.props.user.measurements &&
        Array.isArray(this.props.user.measurements)
      ) {
        EditMeasurementButton = (
          <button
            type="button"
            className="measurement-edit-button"
            onClick={() => {
              !this.state.measurementEditMode
                ? this.turnEditModeOn()
                : this.turnEditModeOff();
            }}
          >
            Delete
          </button>
        );
      }

      return (
        <div className="add view flex column center-all" styles={styles}>
          <div className="view--header flex align-center space-between">
            <h1 className="view--title">{this.state.title}</h1> 
            <div className="form-group form-group-form-controls  buttons flex center-all">
              <button className="make-a-meal" 
                type="button" 
                      onClick={ () => this.props.history.push('make-a-meal') }>Make a meal</button> 
              <button
                className="btn btn-primary save"
                type="button" 
                onClick={this.saveFoodOrDrink}
                disabled={
                  !this.state.steps[0].isValid ||
                  !this.state.steps[1].isValid ||
                  !this.state.steps[2].isValid ||
                  this.props.isSavingNewFoodDrink
                }
              >
                Add
              </button>
              <button
                className="btn btn-primary clear"
                type="button"
                onClick={this.areYouSure}
              >
                Clear
              </button>
              {FormErrors}
            </div>
          </div>
          <div className="view--main">
            <div className="steppers">
              <div className="stepper--stages">
                <div className="stages-container">
                  <div className={`stage ${
                      steps[step - 1].name === "name" ? "stage--active" : ""
                    }`}
                    onClick={() => {
                      this.setStep("name");
                    }}
                  >   
                    <div className={`line ${steps[0].isValid ? 'line-highlighted' : ''}`}></div>
                    {steps[0].isValid ? (
                      <span className="lnr lnr-checkmark-circle" />
                    ) : (
                      <span>Name</span>
                    )}
                  </div>
                  <div
                    className={`stage ${
                      steps[step - 1].name === "type" ? "stage--active" : ""
                    }
                      ${steps[step - 1].isValid ? "stage--valid" : ""}
                      `}
                    onClick={() => {
                      this.setStep("type");
                    }}
                  > 
                    <div className={`line ${steps[1].isValid ? 'line-highlighted' : ''}`}></div>
                    {steps[1].isValid ? (
                      <span className="lnr lnr-checkmark-circle" />
                    ) : (
                      <span>Type</span>
                    )}
                  </div>
                  <div
                    className={`stage ${
                      steps[step - 1].name === "measurements"
                        ? "stage--active"
                        : ""
                    }`}
                    onClick={() => {
                      this.setStep("measurements");
                    }}
                  >
                    <div className={`line ${steps[2].isValid ? 'line-highlighted' : ''}`}></div>
                    {steps[2].isValid ? (
                      <span className="lnr lnr-checkmark-circle" />
                    ) : (
                      <span>Measurements</span>
                    )}
                    </div>
                  <div
                    className={`stage ${
                      steps[step - 1].name === "vitamins" ? "stage--active" : ""
                    }`}
                      onClick={() => {
                        this.setStep("vitamins");
                      }}
                    >
                      Vitamins
                    </div>
                  </div>
              </div>
              <div className={`steppers--content ${step === 4 ? 'steppers--content--large' : ''}`}>
                
                {this.state.step === 1 && (
                  <FoodDrinkInputName
                    name={this.state.foodDrinkInputName}
                    handleOnChange={this.handleFoodDrinkInputOnChange}
                  />
                )}

                {this.state.step === 2 && (
                  <FoodOrDrink
                    foodOrDrink={this.state.foodOrDrink}
                    handleOnChange={this.handleFoodOrDrinkOnChange}
                  />
                )}

                {this.state.step === 3 && (
                  <Measurements 
                    measurements={this.props.user.measurements}
                    selectedMeasurement={this.state.selectedMeasurement}
                    oneMeasurementRequiredMessage={
                      OneMeasurementRequiredMessage
                    }
                    handleOnChange={this.handleMeasurementSelectOnChange}
                  />
                )}

                {this.state.step === 4 && (
                  <Vitamins 
                    state={this.state}
                    vitaminsJSON={vitaminsJSON}
                    outputVitamins={this.outputVitamins}
                    handleOnChange={(event) => {
                      console.log(event.target.name, event.target.value);
                      this.handleOnChange(event);
                     }}
                    oneMeasurementRequiredMessage={
                      OneMeasurementRequiredMessage
                    }
                  >{this.props.children}</Vitamins>
                )}
              </div>
              <div className="stepper--controls flex space-around align-items">
                <Button type="button" disabled={this.state.step === 1} onClick={this.previous} className={`${this.state.step === 1 ? 'disabled' : ''}`}>Previous</Button>
                <Button type="button" disabled={this.state.step === 4} onClick={this.next} className={`${this.state.step === 4 ? 'disabled' : ''}`}>Next</Button>
              </div>
            </div>
            <form>
              {/* Type of food or drink and name*/}
              <div className="form-group type-of-and-name-group flex center-all">
                <div className="form-group--content type-of-and-name flex space-around" />
              </div>
  
              {/* Measurements */}
              {/* <div className="form-group measurements-group"> */}
              {/*   <div className="form-group--content flex space-around"> */}
              {/*     <div> */}
              {/*       <label htmlFor="new-measurement"> */}
              {/*         Need another description? */}
              {/*         <input */}
              {/*           type="text" */}
              {/*           name="new-measurement" */}
              {/*           value={this.state.newMeasurement} */}
              {/*           onChange={this.handleNewMeasurementOnChange} */}
              {/*           id="new-measurement" */}
              {/*           placeholder="Measurement" */}
              {/*         /> */}
              {/*       </label> */}
              {/*       <button */}
              {/*         className="measurement-add-button" */}
              {/*         type="button" */}
              {/*         onClick={this.addNewMeasurement} */}
              {/*       > */}
              {/*         Add */}
              {/*       </button> */}
              {/*       {EditMeasurementButton} */}
              {/*     </div> */}
              {/*   </div> */}
              {/* </div> */}

              {/* Vitamins */}

              {/* Form Controls */}
            </form>

            {/* "Are you sure?" modal */}
            {this.state.showAreYouSure ? (
              <div style={confirmContainerStyles}>
                <AreYouSure answerIs={this.handleAnswer} />
              </div>
            ) : null}
          </div>
        </div>
      );
    } else {
      return (
        <div className="view-with-padding flex center-all color-black">
          <Link to="/auth/signin">Sign In</Link>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    showAlert: state.alertReducers.showAlert,
    user: state.userReducers,
    isAuthenticated: state.userReducers.isAuthenticated,
    isSavingNewFoodDrink: state.dashboardReducers.isSavingNewFoodDrink,
    successSavingNewFoodDrink: state.dashboardReducers.successSavingNewFoodDrink,
    saveNewFoodDrinkErrors: state.userReducers.saveNewFoodDrinkErrors
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
    setAlert: (isVisible, title = "", body = "") => {
      dispatch(setAlert(isVisible, title, body));
    },
    beginSavingNewFoodDrink: foodDrink =>
      dispatch(beginSavingNewFoodDrink(foodDrink)),
    pushToSaveFoodDrinkErrors: errors => {
      dispatch(pushToSaveFoodDrinkErrors(errors));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Add);
