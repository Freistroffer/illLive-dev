import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, configure } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'adapter-enzyme-react-16'; 
import Add from './index.js';


describe('Testing Add component,', function() {
	it('Add component should exist, or it\'s .length value is true', function() {
		const wrapper = shallow(<Add />);

		expect(wrapper.length).toBe(1);
	});
});
