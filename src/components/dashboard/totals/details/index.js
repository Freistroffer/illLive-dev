import React, { Component } from 'react';
import { toggleDetails } from './details_actions';
import { connect } from 'react-redux';
import * as styles from './styles.css';

export class VitaminDetails extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: 'Vitamin Details Modall'
		}
	}

	render() {
		return(
			<div className="vitamin-details-modal center-all" styles={styles}>
				<button onClick={ () => { this.props.toggleDetails(false) } }>X</button>
				<h1>{this.state.title}</h1>
				<h2>{this.props.vitamin}</h2>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		...state
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		toggleDetails: showDetails => { dispatch(toggleDetails(showDetails)) }
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(VitaminDetails);