let mockUser = {
  intake: [
    {
      "id": "vitamin-a",
      "intake": "456"
    },
    {
      "id": "vitamin-b1",
      "intake": "0.9"
    },
    {
      "id":   "vitamin-b2",
      "intake": "1.1"
    },
    {
      "id": "vitamin-b3",
      "intake": "10"
    },
    {
      "id": "vitamin-B5",
      "intake": "3"
    },
    {
      "id": "vitamin-b6",
      "intake": "1.1"
    },
    {
      "id": "vitamin-b7",
      "intake": "23"
    },
    {
      "id": "vitamin-b12",
      "intake": "1.2"
    },
    {
      "id": "calcium",
      "intake": "654"
    },
    {
      "id": "choline",
      "intake": "345"
    },
    {
      "id": "chromium",
      "intake": "23"
    },
    {
      "id": "copper",
      "intake": "500"
    },
    {
      "id": "fluoride",
      "intake": "3"
    },
    {
      "id": "folic acid",
      "intake": "150"
    },
    {
      "id": "iodine",
      "intake": "120"
    },
    {
      "id": "iron",
      "intake": "6"
    },
    {
      "id": "magnesium",
      "intake": "234"
    },
    {
      "id": "manganese",
      "intake": "1.1"
    },
    {
      "id": "molybdenum",
      "intake": "24"
    },
    {
      "id": "phosphorus",
      "intake": "500"
    },
    {
      "id": "potassium",
      "intake": "500"
    },
    {
      "id": "selenium",
      "intake": "24"
    },
    {
      "id": "salt",
      "intake": "500"
    },
    {
      "id": "vitamin-d3",
      "intake": "1500"
    },
    {
      "id": "vitamin-e",
      "intake": "4"
    },
    {
      "id": "vitamin-k",
      "intake": "78"
    },
    {
      "id": "zinc",
      "intake": "5"
    }        
  ]
};