import React, { Component } from 'react';
import user from './lnr-user.svg' ;

export class Vitamin extends Component {

  constructor(props) {
    super(props);

    this.showDetails = this.showDetails.bind(this);
  }

  componentDidMount() {
    console.log(this.props);
  }



  camelCaseWord(vitamin) {
    let words;
    let camelCased;
    let uppercasedWords = [];

    if ( vitamin && vitamin !== undefined ) {
      words = vitamin.split(/\s+/g);
    }
    
    if (words && words.length) {
      words.forEach((word) => {
        uppercasedWords.push(word[0].toUpperCase() + word.substr(1));
      });
    }

    return camelCased = uppercasedWords.join(' ');
  }



  showDetails( vitamin ) {
    alert(vitamin);
  }



  render() {

    const VitaminIcon = (<img alt="Vitamin icon!"  onClick={ () => { this.props.showDetailsModal(this.props.name) } } className="icon" src={user} />);

    let width = (this.props.intake / this.props.vitamin) * 100;
    if (width > 100) width = 100;
    width += '%';   

    const styles = {
      percentage: {
        width,
        background: '#' + this.props.color,
        animation: 'animatePercentage 1s'
      },
      modal: {

      }
    };

    return (
      <div className="vitamin-container">
        <div styles={styles} className="vitamin">
          <div className="percentage" style={styles.percentage}></div>
          <div className="vitamin--name-and-data">
            <div className="vitamin--name">{this.camelCaseWord(this.props.name)}</div>
            <div className="vitamin--data">
              {String(this.props.intake)} out of {this.props.rda}{this.props.measurement}
              {false && VitaminIcon}
            </div>
          </div>
        </div>
      </div>
    );
  }
}



export default Vitamin;
