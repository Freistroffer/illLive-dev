/*
  TODO: on vitamin click if its not at 100% of rda than, reminds me to make it a certain color if its >= 100%, otherwise
  on click, calculate the remaining needed, search in the db for any items containing that vitamina and calculate
  how many/much of that item to equal 100% and what vitamins would it cause to go over 100%. 

  i need 50mg of vit. if i eat 2 pieces of bread it will be 52mg. so it will be like 25 calculations. im at 98 out of 100 vitZ.
  if i eat 2 pieces of bread it will bring me to 132 of vitZ.
  or someway to show all vitamins on a single screen and EMULATE the values based on the clicked item. so i click on a slice of
  bread and it will emulate the vitamin values.
*/

/*
  So yeah get the meals. loop over each meal.foodDrinks as item and item.vitaminName.value 
  
  this.setState({
    [vitaminName]: parseFloat( prevState[vitaminName].vitContent ) += parseFloat(vitamin.vitContent);
  });
*/

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setLoader } from '../../loading/loading_actions'; 
import { toggleDetails } from './details/details_actions';
import Vitamin from './vitamin';
import * as styles from './styles.css';
import * as vitaminsObj from '../../../vitamins.json';


export class Totals extends Component {

  constructor(props) {
    super(props);

    this.state = {
      title: 'My Totals',
      vitaminsUpdated: false,
      selctedVitamin: '',
      showVitaminDetailsModal: false,
      userHasNoMeals: false,
      vitamins: [],
      vitaminA: {
          rda: "900",
          total: 0
      },
      vitaminB1:{
          rda: "1.2",
          total: 0
      },
      vitaminB2: {
          rda: "1.3",
          total: 0
      },
      vitaminB3: {
        rda: "16",
        total: 0
      },
      vitaminB5: {
          rda: "5",
          total: 0
      },
      vitaminB6: {
        rda: "1.3",
        total: 0
      },
      vitaminB7: {
        rda: "30",
        total: 0
      },
      vitaminB12: {
        rda: "2.4",
        total: 0
      },
      calcium: {
          rda: "1000",
          total: 0
      },
      choline: {
        rda: "550",
        total: 0
      },
      chromium: {
        rda: "35",
        total: 0
      },
      copper: { 
        rda: "900",
        total: 0
      },
      fluoride: {
        rda: "4",
        total: 0
      },
      folicAcid: {
        rda: "400",
        total: 0
      },
      iodine: {
        rda: "150",
        total: 0
      },
      iron: {
        rda: "8",
        total: 0
      },
      magnesium: {
        rda: "400",
        total: 0
      },
      manganese: {
        rda: "2.3",
        total: 0
      },
      molybdenum: {
        rda: "45",
        total: 0
      },
      phosphorus: {
        rda: "700",
        total: 0
      },
      potassium: {
        rda: "4700",
        total: 0
      },
      selenium: {
        rda: "55",
        total: 0
      },
      salt: {
        rda: "500",
        total: 0
      },
      vitaminD3: {
        rda: "10000",
        iuValue: "10000",
        total: 0
      },
      vitaminE: {
        rda: "15",
        total: 0
      },
      vitaminK: {
        rda: "120",
        total: 0
      },
      zinc: {
        rda: "11",
        total: 0
      }
    }

    this.showVitaminDetailsModal = this.showVitaminDetailsModal.bind(this);

    document.title = "Totals"
  }



  componentDidMount(){

    this.setState({ 
      vitamins: [vitaminsObj.vitamins]
    });

    const { user  } = this.props;


// 
//     user.meals.forEach(meal => {
//       meal.foodDrinks.forEach(fd => {
//         fd.forEach(vitamin => {
//           
//         });
//       })
//     })

    // TODO shouldn't have to check this
    // App auto routes away if username is empty.
    if (user.isAuthenticated) {
      console.log(this.state.vitamins[0]);
      this.state.vitamins[0].forEach((vitamin, index) => {
        user.meals.forEach((meal, index) => {
          meal.foodDrinks.forEach((foodDrink) => {
            let vitaminToUpdate = this.state[vitamin['inputName']];
            vitaminToUpdate.total = foodDrink[vitamin['inputName']];
            vitaminToUpdate.total = vitaminToUpdate.total * foodDrink.quantity;
            return this.setState({
              [vitamin.inputName]: Object.assign({}, this.state[vitamin['inputName']], vitaminToUpdate)
            }); 
          });
        });
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { user } = this.props;
    if (!prevProps.user.isAuthenticated && user.isAuthenticated) {
      console.log(user);
      this.setState({ 
        vitamins: [vitaminsObj.vitamins]
      }, () => {
        // TODO make it not [0]
        this.state.vitamins[0].forEach((vitamin, index) => {
          return user.meals.map((meal, index) => {
            return meal.foodDrinks.map((foodDrink) => {
              let vitaminToUpdate = this.state[vitamin['inputName']];
              vitaminToUpdate.total = foodDrink[vitamin['inputName']];
              vitaminToUpdate.total = vitaminToUpdate.total * foodDrink.quantity;
              return this.setState({
                [vitamin.inputName]: Object.assign({}, this.state[vitamin['inputName']], vitaminToUpdate)
              }); 
            });
          });
        });
      });
    }
  }



  outputVitamins() {
    // TODO make it not [0]
    console.log(`outputVitamins`, this.state.vitamins);
    return this.state.vitamins[0].map((vitamin, index) => {
      return <Vitamin name={vitamin.name}
                      measurement={vitamin.measurement}
                      intake={ this.state[vitamin.inputName].total }
                      rda={vitamin.rda}
                      color={`28aaa4`}
                      key={index} 
                      showDetailsModal={ this.showVitaminDetailsModal  }
                      onClick={ () => {  this.setState({ selectedVitamin: vitamin.name })  } } /> 
    });
  }



  showVitaminDetailsModal() {
    console.log('[Totals] showVitaminDetailsModal');
    
    this.setState({
      showVitaminDetailsModal: true
    }, () => {

    });
  }



  calculateTotals() {
    this.props.user.consumables.map((consumable, index) => {
      return console.log(consumable);
    });
  }



  // So somehow on click the vitamin value basically redux set the active vitamin or
  // 
  render() {
    const { vitamins, userHasNoMeals } = this.state;
    const { user } = this.props;
    let Vitamins;
    let UserHasNoMealsMessage;
    
    if (userHasNoMeals) {
      UserHasNoMealsMessage = (<div>You haven't made any meals today.</div>);
    } else {
      Vitamins = vitamins && vitamins.length && vitamins.length > 0 ? this.outputVitamins() : [];
    }

    return (
      <div id="totals" className="view center-all" styles={styles}>
        <h1 className="view--title">{ this.state.title }</h1>
        <div className="vitamins-container">
          {UserHasNoMealsMessage}
          {Vitamins}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    ...state,
    user: state.userReducers,
    showDetails: state.detailsReducers.showDetails
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setLoader: (load, text) => dispatch(setLoader(load, text)),
    toggleDetails: showDetails => { dispatch( toggleDetails(showDetails) ) }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Totals);
