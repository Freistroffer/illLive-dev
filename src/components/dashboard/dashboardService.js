import configs from "../../configs";
import axios from "axios";

export const sendSaveFoodDrinkRequest = foodDrink => {
  console.log(`[DashboardService] sendSaveFoodDrinkRequest`, foodDrink);

  return axios(`${configs.apiUrl}/user/add`, {
    method: "post",
    headers: {
      "Content-type": "application/json"
    },
    withCredentials: true,
    data: { foodDrink }
  });
};

export const saveMeals = (username, meals) => {
  console.log(`[DashboardService] saveMeals`, meals);
  return axios.post(`${configs.apiUrl}/user/saveMeals`, { username, meals });
};

export const sendSaveMeasurementRequest = (measurement) => {
  return axios(`${configs.apiUrl}/user/saveMeasurement`, { params: { measurement} });
};

export const sendDeleteMeasurementRequest = (measurement) => {
  return axios(`${configs.apiUrl}/user/deleteMeasurement`, { params: { measurement} });
};