import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setLoader } from '../loading/loading_actions';
import { beginConfirm24HourMealResetMessage } from '../user/user_actions';
import * as styles from './styles.css';


export class Dashboard extends Component {

  constructor(props) {
    super(props);

    document.title = "Dashboard";
  }



  render() {
    // // If they're not signed in than no route to dashboard. Simply check if username is empty
    // // If they're signed in though not confirmed, route to confirm-account
    // //if ( !this.props.accountConfirmed ) return <Redirect to="/signin" />
    // if ( !this.props.user || !this.props.username || this.props.username === "" ) {  
    //   console.log(`[Dashboard] this.props.user.username === ''. Should route to signin`);
    //   <Redirect to="/signin" />
    //   this.props.history.push('/signin');
    // }
    
    return (
      <div className="dashboard view flex column center-all" style={styles}>
        <div className="view--header">
            <h1 className="view--title">Dashboard</h1>
        </div> 

        <div className="view--main">
            <nav className="dashboard--menu flex column justify-start align-start">
              <div className="dashboard--menu--route"><Link to="/me/add" className="flex row space-between align-center">Add a Item<i className="fas fa-apple-alt"></i></Link></div>
              <div className="dashboard--menu--route"><Link to="/me/make-a-meal" className="flex row space-between align-center">Make a meal <i className="fas fa-blender"></i></Link></div>
              <div className="dashboard--menu--route"><Link to="/me/totals" className="flex row space-between align-center">Totals <i className="fas fa-box"></i></Link></div>
            </nav>
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducers,
    accountConfirmed: state.userReducers.accountConfirmed,
    confirm24HourMealReset: state.userReducers.confirm24HourMealResetMessage,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLoader: ( showing, text ) => { dispatch(setLoader(showing, text)) },
    beginConfirm24HourMealReset: () => dispatch(beginConfirm24HourMealResetMessage())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
