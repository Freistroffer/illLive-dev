import React, { Component } from 'react';
import * as styles from './styles.css';

export class Search extends Component {
  render() {
    return (
      <div className="search view center-all" styles={styles}>
        <h1 className="view--title">Search</h1>
      </div>
    );
  }
}

export default Search;
