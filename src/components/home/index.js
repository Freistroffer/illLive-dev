import React, { Component } from 'react';
import * as styles from './styles.css';

export class Home extends Component {
  constructor(props) {
    super(props);
 
    this.routeTo = this.routeTo.bind(this);
  }



  routeTo(route) {
    this.props.history.push(route); 
  }

  render() {
    return(
      <div className="home view-with-padding" styles={styles}>
        <section className="welcome">
          <h1>Record your daily vitamin intake with <br /><span>Visual Charts</span></h1>
          <div className="welcome--buttons">
              <button className="signup-button" type="button" onClick={() => this.routeTo('/auth/signup')}>Sign Up</button>
              <button className="signin-button" type="button" onClick={() => this.routeTo('/auth/signin')}>Sign In</button>
          </div>
        </section>
        <section className="add-vitamins">
          {/*<h1>Add the vitamins from any food or beverage module._initPaths();tem.</h1>*/}
          <h1>Enter the amount of vitamins from any thing whether it be a type of food, or a drink.</h1>
          <h2>Select an item and select a quantity or measurement and visually see how much of a certain vitamin you have consumed this day</h2>
        </section>
        {/*<section className="calculate-your-total">
          <h1>Calculate your daily vitamin intake.</h1>
          <h2>and know what vitamins you may need to </h2>
        </section>*/}
        <section className="mobile-version">
          <h1>Mobile version coming soon!</h1>
        </section>
      </div>
    );
  }
}

export default Home;
