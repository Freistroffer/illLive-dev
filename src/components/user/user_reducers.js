import {
  STORE_USER,
  STORE_AUTHENTICATION_TOKEN,
  STORE_ACCOUNT_CONFIRMATION_TOKEN,
  STORE_API_TOKEN,
  BEGIN_SIGNING_OUT,
  SUCCESS_SIGNING_OUT,
  FAILURE_SIGNING_OUT,
  BEGIN_SIGNING_IN,
  SUCCESS_SIGNING_IN,
  FAILURE_SIGNING_IN,
  BEGIN_SIGNING_UP,
  SUCCESS_SIGNING_UP,
  FAILURE_SIGNING_UP,
  BEGIN_AUTHENTICATING,
  SUCCESS_AUTHENTICATING,
  FAILURE_AUTHENTICATING,
  BEGIN_CONFIRMING_ACCOUNT,
  SUCCESS_CONFIRMING_ACCOUNT,
  FAILURE_CONFIRMING_ACCOUNT,
  SET_CONFIRM_COOKIE_FLAG,
  BEGIN_CONFIRM_24_HOUR_MEAL_RESET,
  SUCCESS_CONFIRM_24_HOUR_MEAL_RESET,
  FAILURE_CONFIRM_24_HOUR_MEAL_RESET
} from "./types";

// TODO change the errors[] to a single Object. Can't think of times when
// multiple errors would exist.
const initialState = {
  username: "",
  email: "",
  password: "", // TODO remove password
  confirmationToken: "",
  authenticationToken: "",
  apiToken: "",
  foodDrinks: [],
  measurements: [],
  errors: [],
  authenticationErrors: [],
  signInErrors: [],
  saveNewFoodDrinkErrors: [],
  confirm24HourMealResetMessageErrors: [],
  accountConfirmationError: {},
  confirmed24HourMealResetMessage: false,
  accountConfirmed: false,
  isAuthenticating: false,
  isAuthenticated: false,
  isSigningIn: false,
  isSigningUp: false,
  isSignedUp: false,
  isSigningOut: false,
  isConfirmingAccount: false,
  isAddingNewFoodDrink: false,
  confirmedCookieFlag: false
};

export const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case STORE_USER:
      return {
        ...state,
        ...action.user
      };

    case BEGIN_AUTHENTICATING:
      return {
        ...state,
        isAuthenticating: true
      };

    case SUCCESS_AUTHENTICATING:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true
      };

    case FAILURE_AUTHENTICATING:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false
      };

    case STORE_ACCOUNT_CONFIRMATION_TOKEN:
      return {
        ...state,
        confirmationToken: action.token
      };

    case STORE_AUTHENTICATION_TOKEN:
      return {
        ...state,
        authenticationToken: action.token
      };

    case STORE_API_TOKEN:
      return {
        ...state,
        apiToken: action.token
      };

    case BEGIN_SIGNING_IN:
      return {
        ...state,
        isSigningIn: true
      };

    case SUCCESS_SIGNING_IN:
      return {
        ...state,
        isSigningIn: false,
        isAuthenticated: true,
        signInErrors: []
      };

    case FAILURE_SIGNING_IN:
      return {
        ...state,
        isSigningIn: false,
        isAuthenticated: false,
        signInErrors: [...action.payload.errors]
      };

    case BEGIN_SIGNING_UP:
      return {
        ...state,
        isSigningUp: true
      };

    case SUCCESS_SIGNING_UP:
      return {
        ...state,
        isSigningUp: false,
        isSignedUp: true
      };

    case FAILURE_SIGNING_UP:
      return {
        ...state,
        isSigningUp: false,
        isSignedUp: false
      };

    case BEGIN_SIGNING_OUT:
      return {
        ...state,
        isSigningOut: true
      };

    case SUCCESS_SIGNING_OUT:
      return {
        ...state,
        username: "",
        email: "",
        password: "",
        confirmationToken: "",
        authenticationToken: "",
        apiToken: "",
        foodDrinks: [],
        measurements: [],
        errors: [],
        authenticationErrors: [],
        signInErrors: [],
        saveNewFoodDrinkErrors: [],
        accountConfirmationError: {},
        accountConfirmed: false,
        isAuthenticating: false,
        isAuthenticated: false,
        isSigningIn: false,
        isSigningUp: false,
        isSigningOut: false,
        isConfirmingAccount: false,
        isAddingNewFoodDrink: false,
        confirmedCookieFlag: false
      };

    case FAILURE_SIGNING_OUT:
      return {
        ...state,
        ...action.user,
        isSigningOut: false
      };

    
   
    case BEGIN_CONFIRMING_ACCOUNT:
      return {
        ...state,
        isConfirmingAccount: true
      };

    case SUCCESS_CONFIRMING_ACCOUNT:
      return {
        ...state,
        isConfirmingAccount: false,
        accountConfirmed: true
      };

    case FAILURE_CONFIRMING_ACCOUNT:
      return {
        ...state,
        isConfirmingAccount: false,
        accountConfirmed: false,
        accountConfirmationError: {
          ...state.accountConfirmationError,
          ...action.payload.error
        }
      };

    case SET_CONFIRM_COOKIE_FLAG:
      return {
        ...state,
        confirmedCookieFlag: true
      };

    case BEGIN_CONFIRM_24_HOUR_MEAL_RESET:
      return {
        ...state
      };

    case SUCCESS_CONFIRM_24_HOUR_MEAL_RESET:
      return {
        ...state,
        confirm24HourMealResetMessage: true
      };

    case FAILURE_CONFIRM_24_HOUR_MEAL_RESET:
      return {
        ...state,
        confirm24HourMealResetMessage: false,
        confirm24HourMealResetMessageErrors: [...action.payload.errors]
      };

    default:
      return { ...state };
  }
};
