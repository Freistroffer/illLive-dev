import {
  STORE_USER,
  STORE_AUTHENTICATION_TOKEN,
  STORE_API_TOKEN,
  BEGIN_SIGNING_IN,
  SUCCESS_SIGNING_IN,
  FAILURE_SIGNING_IN,
  BEGIN_SIGNING_UP,
  SUCCESS_SIGNING_UP,
  FAILURE_SIGNING_UP,
  BEGIN_SIGNING_OUT,
  SUCCESS_SIGNING_OUT,
  FAILURE_SIGNING_OUT,
  BEGIN_AUTHENTICATING,
  SUCCESS_AUTHENTICATING,
  FAILURE_AUTHENTICATING,
  BEGIN_CONFIRMING_ACCOUNT,
  SUCCESS_CONFIRMING_ACCOUNT,
  FAILURE_CONFIRMING_ACCOUNT,
  SET_CONFIRM_COOKIE_FLAG,
  BEGIN_CONFIRM_24_HOUR_MEAL_RESET,
  SUCCESS_CONFIRM_24_HOUR_MEAL_RESET,
  FAILURE_CONFIRM_24_HOUR_MEAL_RESET

} from "./types";
import {
  authenticateUser, // TODO rename to match other requests
  sendConfirmAccountRequest,
  signIn as sendSignInRequest,
  signUp as sendSignUpRequest,
  signOut as sendSignOutRequest,
  sendConfirm24HourMealResetRequest
} from "./userService";
import { deleteStoreState } from "../../utilities/store";
import { addToLocalStorage, clearLocalStorage } from "../../utilities/localStorage";

// Store user in Global Store
// TODO rename as addUserToStore
export let setUser = user => {
  return {
    type: STORE_USER,
    user
  };
};

// Authenticate
export let beginAuthenticating = () => {
  return {
    type: BEGIN_AUTHENTICATING
  };
};

export let authenticate = () => {
  return function(dispatch, getState) {
    dispatch(beginAuthenticating());
    authenticateUser()
      .then((response) => {
        console.log(response);

        switch(response.status) {
          case 500:
            dispatch(failureAuthenticating(response.data));
            break;
          case 401:
            dispatch(failureAuthenticating(response.data));
            break;
          case 200:
            dispatch(successAuthenticating(response.data));
            break; 
          default: 
            dispatch(failureAuthenticating(response.data));
            break;
        }
      })
      .catch(error => {
        dispatch(failureAuthenticating(error));
      });
  };
};

export let successAuthenticating = (user) => {
  return function(dispatch) {
    dispatch(setUser(user));

    if (user.meals && user.meals.length > 0) {
      addToLocalStorage('meals', user.meals);
    }

    return dispatch({
      type: SUCCESS_AUTHENTICATING,
      payload: {
        isAuthenticating: false,
        isAuthenticated: true
      }
    });
  }
};

export let failureAuthenticating = errors => {
  return {
    type: FAILURE_AUTHENTICATING,
    payload: {
      isAuthenticating: false,
      isAuthenticated: false
    }
  };
};

// Store Authentication Token
export let storeAuthenticationToken = token => {
  return {
    type: STORE_AUTHENTICATION_TOKEN,
    token
  };
};

// Store Account Confirmation Token
export let storeAccountConfirmationToken = token => {
  return {
    type: STORE_AUTHENTICATION_TOKEN,
    token
  };
};

// Store API Token
export let storeApiToken = token => {
  return {
    type: STORE_API_TOKEN,
    token
  };
};

// Register a new user
export let beginSigningUp = body => {
  return function(dispatch) {
    // dispatch(signUp(body));

    return dispatch({
      type: BEGIN_SIGNING_UP
    })
  };
};

export let signUp = (body) => {
  return async function(dispatch) {
    try {
      const signUpResponse = await sendSignUpRequest(body);
      if (signUpResponse.data.success) {
        dispatch(successSigningUp());
      } else {
        dispatch(failureSigningUp(signUpResponse.data.data));
      }
    } catch(error) {
      dispatch(failureSigningUp(error));
    }
  };
};

export let successSigningUp = user => {
  return function(dispatch) {
    // Is this required?
    // dispatch(setUser(user));
    return dispatch({
      type: SUCCESS_SIGNING_UP
    });
  };
};

export let failureSigningUp = () => {
  return {
    type: FAILURE_SIGNING_UP
  };
};

// Signing in
export let beginSigningIn = credentials => {
  return function(dispatch) {
    dispatch(signIn(credentials));

    dispatch({
      type: BEGIN_SIGNING_IN,
      payload: {
        isSigningIn: true
      }
    })
  };
};

export let signIn = (credentials) => {
  return async function(dispatch) {
    try {
      const { status, data } = await sendSignInRequest(credentials);

      if (status === 200) {
        dispatch(successSigningIn(data.data));
      } else {
        dispatch(failureSigningIn(data.data));
      }
    } catch(errors) {
      console.warn(`An error occured sendingSignInRequest`, errors);  
      dispatch(failureSigningIn(errors));
    }
  };
};

export let successSigningIn = user => {
  return function(dispatch) {
    dispatch(setUser(user));

    if (user.meals && user.meals.length > 0) {
      addToLocalStorage('meals', user.meals);
    }
    
    dispatch({
      type: SUCCESS_SIGNING_IN
    });
  };
};

export let failureSigningIn = (errors) => {
  return {
    type: FAILURE_SIGNING_IN,
    payload: {
      errors: [...errors]
    }
  };
};

// Signing out
export let beginSigningOut = () => {
  return function(dispatch) {
    dispatch(signOut());

    return dispatch({
      type: BEGIN_SIGNING_OUT,
      payload: {
        isSigningOut: true
      }
    })
  };
};

export let signOut = () => {
  return async function(dispatch) {
    deleteStoreState();
    clearLocalStorage();

    const response = await sendSignOutRequest();

    if (response.status === 200) {
      dispatch(successSigningOut(response.data));
    } else {
      dispatch(failureSigningOut(response.data));
    }
  };
};

export let successSigningOut = (success) => {
  return {
    type: SUCCESS_SIGNING_OUT
  };
};

export let failureSigningOut = (errors) => {
  return {
    type: FAILURE_SIGNING_OUT
  };
};



// Confirm account
export let beginConfirmingAccount = token => {
  return function(dispatch) {
    dispatch(confirmAccount(token));

    return dispatch({
      type: BEGIN_CONFIRMING_ACCOUNT
    });
  };
};

export let confirmAccount = token => {
  return function(dispatch) {
    sendConfirmAccountRequest(token)
      .then(response => {
        const { invalidToken, tokenExpired } = response.data;
        let errorObj = {};

        if (response.status === (401 || 422)) {
          errorObj["invalidToken"] = invalidToken;
          errorObj["tokenExpired"] = tokenExpired;
          dispatch(failureConfirmingAccount(errorObj));
        } else if (response.status === 201) {
          dispatch(successConfirmingAccount());
        }
      })
      .catch(error => {
        console.warn(
          '[ConfirmAccount] error POSTing auth/resend-confirmation', error
        );
        dispatch(failureConfirmingAccount(error));
      });
  };
};

export let successConfirmingAccount = () => {
  return {
    type: SUCCESS_CONFIRMING_ACCOUNT
  };
};

export let failureConfirmingAccount = error => {
  return {
    type: FAILURE_CONFIRMING_ACCOUNT,
    payload: {
      error
    }
  };
};

// Conform cookie flag
export let setConfirmCookieFlag = () => {
  return {
    type: SET_CONFIRM_COOKIE_FLAG
  };
};

export let beginConfirm24HourMealResetMessage = () => {
  return {
    type: BEGIN_CONFIRM_24_HOUR_MEAL_RESET
  }
}

export let confirm24HourMealResetMessage = () => {
  return async function(dispatch) {
    try {
       const response = await sendConfirm24HourMealResetRequest();

       if (response.success === 200) {
         dispatch(successConfirm24HourMealResetMessage());
       } else {
         dispatch(successConfirm24HourMealResetMessage(response.data.errors));
       }
    } catch(error) {
         dispatch(successConfirm24HourMealResetMessage(error));
    }
  }
};

export let successConfirm24HourMealResetMessage = () => {
  return {
    type: SUCCESS_CONFIRM_24_HOUR_MEAL_RESET
  }
}

export let failureConfirm24HourMealResetMessage = (errors) => {
  return {
    type: FAILURE_CONFIRM_24_HOUR_MEAL_RESET,
    payload: {
      errors
    }
  }
}

