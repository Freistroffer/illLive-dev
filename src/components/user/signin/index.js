import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import { Input } from 'antd';
import {
  setUser,
  storeUserInLocalStorage,
  beginSigningIn
} from "../user_actions";
import { setLoader } from "../../loading/loading_actions";
import * as signInStyles from "./styles.css";

export class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formSubmitted: false,
      signInSuccessfull: false,
      rememberMe: false,
      formErrorText: "",
      loginErrorText: "",
      passwordErrorText: "",
      login: "",
      password: ""
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRememberMeOnChange = this.handleRememberMeOnChange.bind(this);

    document.title = "Sign In";
  }


  createInputValidationVariables(event) {
    const name = event.target.name;
    const capitalizedInputName = `${name[0].toUpperCase() + name.substr(1)}`;
    const value = event.target.value.replace(" ", "");
    const inputIsEmpty = value.length === 0 ? true : false;
    const inputNameErrorText = `${name}ErrorText`;

    return {
      name,
      capitalizedInputName,
      value,
      inputIsEmpty,
      inputNameErrorText
    };
  }

  inputsAreAllPopulated() {
    const loginValue = document
      .getElementById("signin-login")
      .value.replace(" ", "");
    const passwordValue = document
      .getElementById("signin-password")
      .value.replace(" ", "");

    return (
      this.state.login.replace(" ", "") !== "" &&
      loginValue !== "" &&
      this.state.password.replace(" ", "") !== "" &&
      passwordValue !== ""
    );
  }

  // TODO rename or add specific onChange for each unique event
  handleOnChange(event) {
    const {
      name,
      capitalizedInputName,
      value,
      inputIsEmpty,
      inputNameErrorText
    } = this.createInputValidationVariables(event);

    // Sets the inputs value as the state value and if it's empty it outputs a error message.
    this.setState({
      formErrorText: "",
      [name]: value,
      [inputNameErrorText]: inputIsEmpty
        ? `${capitalizedInputName} is required.`
        : ""
    });
  }

  handleRememberMeOnChange() {
    this.setState({
      rememberMe: !this.state.rememberMe
    });
  }

  handleOnBlur(event) {
    const {
      capitalizedInputName,
      inputIsEmpty,
      inputNameErrorText
    } = this.createInputValidationVariables(event);

    // Handles empty values for all inputs
    this.setState({
      [inputNameErrorText]: inputIsEmpty
        ? `${capitalizedInputName} is required.`
        : ""
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    // TODO IF the user is already signed in,
    //      show a alert thing letting them know
    //      they're already signed in. BYe.

    // If all inputs are populated ...
    if (this.inputsAreAllPopulated()) {
      // Show spinner icon
      this.setState({ formSubmitted: true });

      const credentials = {
        login: this.state.login,
        password: this.state.password,
        rememberMe: this.state.rememberMe
      };

      this.props.beginSigningIn(credentials);

      // Otherwise find the empty inputs
    } else {
      const stateLoginValue = this.state.login.replace(" ", "");
      const statePasswordValue = this.state.password.replace(" ", "");
      const loginValue = document
        .getElementById("signin-login")
        .value.replace(" ", "");
      const passwordValue = document
        .getElementById("signin-password")
        .value.replace(" ", "");

      // Is the username/email input empty?
      this.setState({
        loginErrorText:
          stateLoginValue === "" || loginValue === ""
            ? "Login is required."
            : ""
      });
      // Is the password input empty?
      this.setState({
        passwordErrorText:
          statePasswordValue === "" || passwordValue === ""
            ? "Password is required."
            : ""
      });
    }
  }

  render() {
    let formErrorText, loginErrorText, passwordErrorText;
    let state = this.state;

    // Create a form error text div with the error if it's errorState is not an empty string
    if (state.loginErrorText !== "") {
      loginErrorText = (
        <div className="form-error-text">{state.loginErrorText}</div>
      );
    }

    if (state.passwordErrorText !== "") {
      passwordErrorText = (
        <div className="form-error-text">{state.passwordErrorText}</div>
      );
    }

    if (state.formErrorText !== "") {
      formErrorText = <div className="form-error">{state.formErrorText}</div>;
    }

    return (
      <div
        className="signin flex column view-with-padding center-all"
        styles={signInStyles}
      >
        <h1 className="view--title">Sign In</h1>

        <form
          className={`signin--form ${
            this.state.validForm ? "display-errors" : ""
          }`}
          onSubmit={this.handleSubmit}
          noValidate
        >
          {formErrorText}

          {/* Login */}
          <div className="form-group">
            <Input
              name="login"
              type="text"
              id="signin-login"
              className={
                this.state.loginErrorText !== "" ? "form-error-input" : ""
              }
              placeholder="Username or email"
              autoComplete="on"
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required
            />
            {loginErrorText}
          </div>

          {/* Password */}
          <div className="form-group">
            <Input
              name="password"
              type="password"
              id="signin-password"
              className={
                this.state.passwordErrorText !== "" ? "form-error-input" : ""
              }
              placeholder="Password"
              autoComplete="off"
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required
            />
            {passwordErrorText}
          </div>

          {/* Stay Signed In */}
          <div className="form-group">
            <label htmlFor="stay-signed-in">
              Stay Signed In?
              <input
                onChange={this.handleRememberMeOnChange}
                type="checkbox"
                name="stay-logged-in"
                id="stay-signed-in"
              />
            </label>
          </div>

          <button type="submit">
            {this.state.formSubmitted && this.props.isSigningIn && (
              <i className="fa fa-spinner fa-pulse" />
            )}
            {!this.state.formSubmitted && !this.props.isSigningIn && <div>Go</div>}
          </button>
          <h6 className="link-to-opposite-form">
            <Link to="/auth/signup">
              Need an account? <span> Sign up here.</span>
            </Link>
          </h6>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userReducers,
    isSigningIn: state.userReducers.isSigningIn
  };
};

const mapDispatchToProps = dispatch => {
  return {
    beginSigningIn: (credentials) => dispatch(beginSigningIn(credentials)),
    setLoader: (isLoading = false, loadingText = "Loading ...") =>
      dispatch(setLoader(isLoading, loadingText)),
    setUser: user => {
      dispatch(setUser(user));
    },
    storeUserInLocalStorage: user => dispatch(storeUserInLocalStorage(user))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SignIn)
);
