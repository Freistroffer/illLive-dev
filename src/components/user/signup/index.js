import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { setUser, beginSigningUp, successSigningUp, failureSigningUp } from '../user_actions';
import { signUp as sendSignUpRequest } from '../userService';
import { setLoader } from '../../loading/loading_actions';
import * as signUpStyles from './styles.css';
import { 
  EMAIL_REGEX, 
  INVALID_EMAIL_TEXT, 
  PASSWORDS_MATCH_ERROR_TEXT 
} from './constants';

export class SignUp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      formSubmitted: false,
      signUpSuccessfull: false,
      formErrorText: '',
      usernameErrorText: '',
      emailErrorText: '',
      passwordErrorText: '',
      confirmErrorText: '',
      username: '',
      email: '',
      password: '',
      confirm: ''
    }

    this.handleOnBlur   = this.handleOnBlur.bind(this);
    this.handleSubmit   = this.handleSubmit.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);

    document.title = "Signup";
  }



  // Returns basic features ( name, value, is it empty?, [name of input]ErrorText)
  createInputValidationVariables(event) {
    // Name of the input
    const name = event.target.name;
    // Formats the name of the input to show error message
    const capitalizedInputName =
      name !== "confirm"  ?
      `${name[0].toUpperCase() + name.substr(1)}` :
      "Your password confirmation";
    // Value of the input plus white space removed
    const value = event.target.value.replace(" ", "");
    // Checks if the input is empty
    const inputIsEmpty = value.length === 0 ? true : false;
    // Sets the error text name for the current input
    const inputNameErrorText = `${name}ErrorText`;

    return {
      name,
      capitalizedInputName,
      value,
      inputIsEmpty,
      inputNameErrorText
    }
  }



  inputsAreAllPopulated() {
    console.log(`inputsAreAllPopulated()`);
    // TODO: I just added the document.getElem ... test if it works.
    let state = this.state;
    let username = document.getElementById('signup-username').value.replace(" ", "");
    let email = document.getElementById('signup-email').value.replace(" ", "");
    let password = document.getElementById('signup-password').value.replace(" ", "");
    let confirm = document.getElementById('signup-confirm-password').value.replace(" ", "");

    return (
             state.username.replace(" ", "") && username &&
             state.email.replace(" ", "")    && email    &&
             state.password.replace(" ", "") && password &&
             state.confirm.replace(" ", "")  && confirm !== ""
           );
  }



  emailIsValid() {
    console.log(`emailIsValid()`);
	   return this.state.email.match(EMAIL_REGEX);
  }



  passwordsMatch() {
    console.log(`passwordsMatch()`);
    const state = this.state;
    return state.password.replace(" ", "") === state.confirm.replace(" ", "");
  }



  handleOnChange (event) {
    const { name, capitalizedInputName, value, inputIsEmpty, inputNameErrorText } = this.createInputValidationVariables(event);

    // Sets the inputs value as the state value and if it's empty it outputs a error message.
    this.setState({
      formErrorText: "",
      [name]: value,
      [inputNameErrorText]: inputIsEmpty ? `${capitalizedInputName} is required.` : ''
    });

    // If the user is typing in the password input ...
    if ( name === "password" ) {
      // And the confirm password is showing the passwords must match error,
      // or confirm password is not an empty string
      if (
          this.state.confirmErrorText === PASSWORDS_MATCH_ERROR_TEXT ||
          this.state.confirm.replace(" ", "") !== ""
        ) {

        // If they passwords end up matching, remove all errors
        if ( value ===  this.state.confirm.replace(" ", "") ) {
          this.setState({
            passwordErrorText: '',
            confirmErrorText: ''
          });

        // Else show the PASSWORDS_MATCH_ERROR_TEXT
        } else {
          this.setState({
            passwordErrorText: PASSWORDS_MATCH_ERROR_TEXT
          });
        }
      }
    }

    // If the user is typing in the confirm-password input ...
    if ( name === "confirm" ) {
      // And the confirm password is showing the passwords must match error,
      // or confirm password is not an empty string
      if ( this.state.passwordErrorText === PASSWORDS_MATCH_ERROR_TEXT &&
           this.state.password.replace(" ", "") !== "") {
        // If they match, remove all errors
        if ( value ===  this.state.password.replace(" ", "") ) {
          this.setState({
            passwordErrorText: '',
            confirmErrorText: ''
          });

        // Else show the PASSWORDS_MATCH_ERROR_TEXT
        } else {
        this.setState({
          confirmErrorText: PASSWORDS_MATCH_ERROR_TEXT
        });
      }
      }
    }
  }



  handleOnBlur(event) {
    const { name, capitalizedInputName, value, inputIsEmpty, inputNameErrorText } = this.createInputValidationVariables(event);

    // Handles empty values for all inputs
    if ( inputIsEmpty ) {
      this.setState({
        [inputNameErrorText]: inputIsEmpty ? `${capitalizedInputName} is required.` : ''
      });

    // Handles email, password, and confirm password validations
    } else {
      const passwordValue = document.getElementById('signup-password').value.replace(" ", "");
      const confirmValue  = document.getElementById('signup-confirm-password').value.replace(" ", "");

      // Email validation
      if (name === 'email') {
        this.setState({
          emailErrorText: !value.match(EMAIL_REGEX) ? 'Enter a valid email.' : ''
        });
      }

      // Idea is logically the user would enter a password THAN the confirm password input.
      // So if the user blurs confirm, it checks if the passwords match and outputs an error if they don't.
      if ( name === 'confirm' ) {
        this.setState({
          confirmErrorText: passwordValue !== confirmValue ? PASSWORDS_MATCH_ERROR_TEXT : ''
        });
      }
    }
  }



  handleSubmit(event) {
    event.preventDefault();

    const state = this.state;

    if ( this.inputsAreAllPopulated() ) { 
      if ( this.emailIsValid() && this.passwordsMatch() ) {
        try {
          this.setState({ formSubmitted: true }, async () => {    
            
            const body = {
              username: state.username,
              email: state.email,
              password: state.password
            };

            this.props.beginSigningUp();

            // POST to api/signup
            const signUpResponse  = await sendSignUpRequest(body);
            const { status, data: { success, message, data: responseData, usernameTaken, emailTaken }} = signUpResponse;

            console.log(status, success, message, responseData);

            // TODO redo.  
            if (status === 201) {
              this.props.successSigningUp();

            } else {
              //this.props.failureSignUp();
              if ( usernameTaken || emailTaken ) {
                this.setState({
                  formErrorText: message,
                  formSubmitted: false
                }, () => {
                  this.props.setLoader(false);
                });
              }      
            }          
          });
        } catch(error) {

        } 
      } else {
        if ( !this.emailIsValid() ) {
          this.setState({ 
            emailErrorText: !this.emailIsValid() ? INVALID_EMAIL_TEXT : this.state.emailErrorText,
            confirmErrorText: !this.passwordsMatch() ? PASSWORDS_MATCH_ERROR_TEXT : this.state.confirmErrorText
          });
        }
      }

    } else {
	  // Output specific error texts
      const usernameValue = state.username.replace(" ", "");
      const emailValue    = state.email.replace(" ", "");
      const passwordValue = state.password.replace(" ", "");
      const confirmValue  = state.confirm.replace(" ", "");

      if ( usernameValue === "" && emailValue    === "" &&
           passwordValue === "" && confirmValue  === "" ) {

          this.setState({ formErrorText: "Fill out the form." });
      } else {
        if (usernameValue === "") {
          this.setState({ 
            usernameErrorText: usernameValue === "" ? "Username is required." : this.state.usernameErrorText,
            emailErrorText:    emailValue    === "" ? "Email is required." : this.state.emailErrorText,
            passwordErrorText: passwordValue === "" ? "Password is required." : this.state.passwordValue === "",
            confirmErrorText:  confirmValue  === "" ? "Confirm password is required." : this.state.confirmErrorText
          });
        }
      }
    }
  }



  render() {

    let formError,
        usernameErrorText,
        emailErrorText,
        passwordErrorText,
        confirmErrorText;
    let state = this.state;

    // Create a form error text div with the error if it's errorState is not an empty string
    if (state.usernameErrorText !== "") {
      usernameErrorText = <div className="form-error-text flex center-all">{state.usernameErrorText}</div>
    }

    if (state.emailErrorText !== "") {
      emailErrorText = <div className="form-error-text flex center-all">{state.emailErrorText}</div>
    }

    if (state.passwordErrorText !== "") {
      passwordErrorText = <div className="form-error-text flex center-all">{state.passwordErrorText}</div>
    }

    if (state.confirmErrorText !== "") {
      confirmErrorText = <div className="form-error-text flex center-all">{state.confirmErrorText}</div>
    }

    if (state.formErrorText !== "") {
      formError = <div className="form-error">{state.formErrorText}</div>
    }

    return(
      <div className="signup view-with-padding flex column center-all" styles={signUpStyles}>
        <h1 className="view--title-mobile">Account Setup</h1>
        <h1 className="view--title">Create your account</h1>
        <form className={`signup--form ${this.state.validForm ? 'display-errors' : '' }` }
              onSubmit={this.handleSubmit}
              noValidate>
          {formError}

          {/* Username */}
          <div className="form-group">
            <label htmlFor="signup-username">Username</label>
            <input
              name="username" type="text"
              id="signup-username"
              className={this.state.usernameErrorText !== '' ? 'form-error-input' : ''}
              placeholder="Username" autoComplete="on"
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required/>
            {usernameErrorText}
          </div>

          {/* Email */}
          <div className="form-group">
            <label htmlFor="signup-email">Email</label>
            <input
              name="email" type="text"
              id="signup-email"
              className={this.state.emailErrorText !== '' ? 'form-error-input' : ''}
              placeholder="Email" autoComplete="email"
              pattern={this.EMAIL_REGEX}
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required/>
            {emailErrorText}
          </div>

          {/* Password */}
          <div className="form-group">
            <label htmlFor="signup-password">Password</label>
            <input
              name="password" type="password"
              id="signup-password"
              className={this.state.passwordErrorText !== '' ? 'form-error-input' : ''}
              placeholder="Password" autoComplete="off"
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required/>
            {passwordErrorText}
          </div>

          {/* Confirm Password */}
          <div className="form-group">
            <label htmlFor="signup-confirm-password">Confirm Password</label>
            <input
              name="confirm" type="password"
              id="signup-confirm-password"
              className={this.state.confirmErrorText !== '' ? 'form-error-input' : ''}
              placeholder="Confirm your password" autoComplete="off"
              onChange={this.handleOnChange}
              onBlur={this.handleOnBlur}
              required/>
            {confirmErrorText}
          </div>


          <button type="submit" onClick={this.onSubmit}>
            { this.state.formSubmitted && <i className="fa fa-spinner fa-pulse"></i> }
            { !this.state.formSubmitted && "All Set" }
          </button>

          <h6 className="link-to-opposite-form"><Link to="/auth/signin">Already a member? <span> Sign in here.</span></Link></h6>
        </form>
      </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    ...state
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    beginSigningUp: () => dispatch(beginSigningUp()),
    successSigningUp: () => dispatch(successSigningUp()),
    failureSigningUp: () => dispatch(failureSigningUp()),
    setLoader: (isLoading = false, loadingText = "Loading ...") => dispatch(setLoader(isLoading, loadingText)),
    setUser: user => dispatch(setUser(user)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
