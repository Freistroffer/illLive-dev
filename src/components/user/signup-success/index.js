import React, { useEffect } from 'react';
import * as successStyles from './styles.css';

const SignUpSuccess = (props) => {
	useEffect(() => {
		document.title = "Signup Success!"
	});		

	return(
		<div className="signup-success view-with-padding flex column center-all" styles={successStyles}>
		  <h1>Nice! Your Account is created!</h1>
		  <h2>An email is being sent for you to confirm your account.</h2>
		  <button type="button" onClick={ () => { props.history.push('/auth/signin') } }>Sign In</button>
		</div>
	);
};

export default SignUpSuccess;
