import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import { beginSigningOut } from "../../components/user/user_actions";
import { setLoader } from "../../components/loading/loading_actions";
import { toggleMenu } from "../../components/menu/menu_actions";
import { Menu } from "./components/menu";
import * as headerStyles from "./styles.css";

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      communicationType: "message",
      menuOpen: false,
      userSelectedSignOut: false,
      userMenuIsVisible: false
    };
  }

  signOut = () => {
    this.props.signOut();
  };

  closeMenu = () => {
    this.setState({ userMenuIsVisible: false });
  };

  render() {
    const { user, siteName, menuOpen } = this.props;

    // TODO what is this?
    if (this.state.userSelectedSignOut) {
      return <Redirect to="/auth/signin" />;
    }

    let IsAuthenticatedHeaderItems;
    let NotAuthenticatedRoutes;

    if (!user.isAuthenticated) {
      NotAuthenticatedRoutes = (
        <nav className="header--menu flex space-around align-center">
          <Link to="/auth/signup">Sign Up</Link>
          <Link to="/auth/signin">Sign In</Link>
        </nav>
      );
    } else {
      IsAuthenticatedHeaderItems = (
        <nav className="header--menu">
          <h2
            className="header--user-name"
            onMouseLeave={() => {
              this.setState({ userMenuIsVisible: false });
            }}
            onClick={() => {
              this.setState({
                userMenuIsVisible: !this.state.userMenuIsVisible
              });
            }}
          >
            {user.username}
            <i className="fa fa-caret-down" />
            {this.state.userMenuIsVisible && (
              <Menu signOut={this.signOut} closeMenu={this.closeMenu} />
            )}
          </h2>
          <Link to="/me">Dashboard</Link>
        </nav>
      );
    }

    return (
      <header className="header" styles={headerStyles}>
        <h1 className="header--site-name">
          <Link to="/">{siteName}</Link>
        </h1>

        <div className="flex row">
          {IsAuthenticatedHeaderItems}
          {NotAuthenticatedRoutes}
          <div
            className={`header--menu-toggler ${
              menuOpen ? "menu-toggler-fixed" : ""
            } `}
            onClick={this.props.toggleMenu}
          >
            <div className={`toggler ${menuOpen ? "open" : ""}`}>
              <span className={`${menuOpen ? "background-white" : ""} `} />
              <span className={`${menuOpen ? "background-white" : ""} `} />
              <span className={`${menuOpen ? "background-white" : ""} `} />
              <span className={`${menuOpen ? "background-white" : ""} `} />
            </div>
          </div>
        </div>
        <div className="header--border" />
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    menuOpen: state.menuReducers.menuOpen,
    user: state.userReducers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLoader: (isLoading, loadingText) =>
      dispatch(setLoader(isLoading, loadingText)),
    toggleMenu: () => dispatch(toggleMenu()),
    signOut: () => dispatch(beginSigningOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
