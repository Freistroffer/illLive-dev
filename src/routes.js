import Home from './components/home';
import SignUp from './components/user/signup';
import SignIn from './components/user/signin';
import SetProfileAfterSignup from './components/user/profile-after-signup';
import Dashboard from './components/dashboard';
import NewConsumable from './components/dashboard/consumables/add';
import EditConsumable from './components/dashboard/consumables/edit';
import Totals from './components/dashboard/totals'; 
import Consumed from './components/dashboard/consumed';  
import MakeAMeal from './components/dashboard/make-a-meal';


export const notAuthenticatedRoutes = [ 
  {
    name: 'Home',
    path: '/',
    component: Home
  },
  {
    name: 'Sign Up',
    path: '/auth/signup',
    component: SignUp
  },
  {
    name: 'Sign In',
    path: '/auth/signin',
    component: SignIn
  },
  {
    name: 'Intro Profile',
    path: '/auth/intro-profile',
    component: SetProfileAfterSignup
  }
];

export const authenticatedRoutes = [
  {
    name: 'Home',
    path: '/',
    component: Home
  },
  // {
  //   name: 'Search',
  //   path: '/search',
  //   component: Search
  // },
  {
    name: 'Dashboard',
    path: '/me',
    component: Dashboard
  },
  {
    name: 'Totals',
    path: '/me/totals',
    component: Totals
  }, 
  {
    name: 'Make A Meal',
    path: '/me/make-a-meal',
    component: MakeAMeal
  },

  // {
  //   name: 'Details',
  //   path: '/totals/details',
  //   component: Details
  // },  
  // {
  //   name: 'Vitamins',
  //   path: '/dashboard/totals/vitamins',
  //   component: Vitamins
  // },
  {
    name: 'Add',
    path: '/me/add',
    component: NewConsumable
  },  {
    name: 'Edit',
    path: '/me/edit',
    component: EditConsumable
  },
  {
    name: 'Consumed',
    path: '/me/consumed',
    component: Consumed
  }
];