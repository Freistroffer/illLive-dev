function isAuthenticated(req, res, next) {
    if (req.session.userID) {
        next();
    } else {
        res.status(404).json({
            data: 'not authenticated'
        });
    }
}

module.exports = isAuthenticated;