// TODO what is this file considered? utility? middleware? ???

function clientErrorHandler(err, req, res, next) {
	if (req.xhr) {
		console.log(`clientErrorHandler() req.xhr`, err);
		res.status(500).send({
			success: false,
			message: 'An error occured',
			data: {
				error: err
			}
		});
	} else {
		next(err);
	}
}

function errorHandler(err, req, res, next) {
	console.log(`errorHandler() err`, err);
	res.status(500).send({
		success: false,
		message: 'An error occured',
		data: {
			error: err
		}
	});
}


module.exports = {
	clientErrorHandler,
	errorHandler
}