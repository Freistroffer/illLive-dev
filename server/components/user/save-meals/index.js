const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const User = require('../../auth/user.model');
const securedUserResponseObject = require('../../auth/securedUserResponseObject');


router.post('/', async (req, res) => {
	console.log(req.session);
	try {
		const { meals } = req.body;

		const { error, user } = await User.findOne({ _id: req.session.userID });

		if (error) {
			console.log(error);
			res.status(500).json({ data: error });
		}

		if (!user) {
			console.log('!user')
			res.status(401).json({ data: 'no user found' });
		}

		user.find(
			{'meals': { '$in': meals }},
			(error, documents) => {
				if (error) {
					res.status(500).json({ data: error });
				}

				if (!documents) {
					res.status(401).json({ data: 'no user found' });
				}

				res.status(200).json({ data: documents });
			}
		);
	} catch(error) {

		console.log(`catch block`, error);
		res.status(500).json({ data: error });
	}
});


module.exports = router;