const router = require('express').Router();
const User = require('../../auth/user.model');
const { FIND_USER_ERROR, SAVED_USER_ERROR  } = require('./constants');
const securedUserResponseObject = require('../../auth/securedUserResponseObject');

router.post('/', (req, res) => {
	console.log(`/user/add reached`);
	console.log('req.session', req.session);

	try {
		const { details, vitamin } = req.body.foodDrink; 

		const foodDrink = {
			name: details.name,
			foodOrDrink: details.foodOrDrink,
			selectedMeasurement: details.selectedMeasurement,
		  	vitaminA:   vitamin.vitaminA,
			vitaminB1:  vitamin.vitaminB1,
			vitaminB2:  vitamin.vitaminB2,
			vitaminB3:  vitamin.vitaminB3,
			vitaminB5:  vitamin.vitaminB5,
			vitaminB6:  vitamin.vitaminB6,
			vitaminB7:  vitamin.vitaminB7,
			vitaminB12: vitamin.vitaminB12,
			calcium:    vitamin.calcium,
			choline:    vitamin.choline,
			chromium:   vitamin.chromium,
			copper:     vitamin.copper,
			flouride:   vitamin.flouride,
			folicAcid:  vitamin.folicAcid,
			iodine:     vitamin.iodine,
			iron:       vitamin.iron,
			magnesium:  vitamin.magnesium,
			molybdenum: vitamin.molybdenum,
			phosphorus: vitamin.phosphorus,
			potassium:  vitamin.potassium,
			selenium:   vitamin.selenium,
			salt:       vitamin.salt,
			vitaminD3:  vitamin.vitaminD3,
			vitaminE:   vitamin.vitaminE,
			vitaminK:   vitamin.vitaminK,
			zinc:       vitamin.zinc
	};


		console.log(foodDrink);

		User.findOneAndUpdate(
			{ _id: req.session.userID },
			{ 
				$push: {
					foodDrinks: foodDrink
				},
				$set: {
					measurements: details.measurements
				}
			},
			{ new: 'true' },
			(error, savedUser) => {

				if (error) {
					console.log(`An error occured finding a User`, error);
					res.status(500).json({
						success: false,
						message: 'An error occured saving the item. Try again, please.',
						dataDescription: "{ error } error returned",
						data: {
							error
						}
					});
				}

				if (savedUser) {
					console.log(`savedUser`, savedUser);
					req.session.user = securedUserResponseObject(savedUser);
					console.log(req.session.user);
					res.status(200).json({
						success: true,
						message: 'Item saved.',
						dataDescription: "savedUser",
						data: {
							savedUser: req.session.user
						}
					});
				}
			});
		} catch(error) {
			res.status(500).json({
				success: false,
				message: 'Error',
				data: error
			})
		}
});

module.exports = router;