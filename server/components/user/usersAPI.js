const express = require('express');
const router = express.Router();
const add = require('./add'); 
const edit = require('./edit');
const saveMeals = require('./save-meals');
const saveMeasurement = require('./saveMeasurement');
const deleteMeasurement = require('./deleteMeasurement');

router.use('/add', add);
router.use('/edit', edit);
router.use('/saveMeals', saveMeals);
router.use('/saveMeasurement', saveMeasurement);
router.use('/deleteMeasurement', deleteMeasurement);

module.exports = router;
