const router = require("express").Router();
const User = require("../user.model");
const securedUserResponseObject = require('../securedUserResponseObject');

router.get("/", async (req, res) => {
  console.log(`/confir-24-hour-meal-reset`);

  try {
    const result = await User.findOneAndUpdate(
      { _id: req.session.userID },
      { $set: { confirmed24HourMealResetMessage: true } },
      { new: true }
    );

    console.log(result);

    res.status(200).json({});

  } catch(error) {
    res.status(500).json({
      data: error
    });
  }
});

module.exports = router;
