const router = require("express").Router();
const bcrypt = require("bcryptjs");
const User = require("../user.model");
const { FIND_USER_ERROR, COMPARE_PASSWORD_ERROR } = require("../constants");
const { comparePasswords } = require("../authServices");
const securedUserResponseObject = require("../securedUserResponseObject");

async function findUser(req, res) {
  const { login, password, rememberMe } = req.body;
  try {
    let foundUser = await User.findOne({ $or: [{ username: login }, { email: login }] });

    console.log(foundUser);
    
    if (foundUser) {
      return { success: true, data: foundUser };
    } else {
      return { success: false, data: 'no user found'}
    }
     
  } catch(error) {
    return { success: false, data: error }
  }
}

router.post("/", async (req, res, next) => {
  try {
    const { login, password, rememberMe } = req.body;
    const { success, data } = await findUser(req, res);

    console.log(`success data`, success, data);

    if (!success) {
      return res.status(401).json({
        success: false,
        data
      });
    }

    comparePasswords(password, data.password).then(
      (validPassword, error) => {
        console.log(
          `in .then() after comparePasswords`,
          error,
          validPassword
        );

        // Error validating passwords
        if (error) {
          res.status(500).json({
            success: false,
            message: 'error validating password',
            dataDescription: 'error from validating password',
            data: error
          });
        }

        if (validPassword) {
          req.session.userID = data._id;

          if (rememberMe) {
            req.session.cookie.maxAge = 2628000000;
          }

          console.log(`SINGINIGN IN SEESESSISONSSN `, req.session.userID);

          // Success or fail response sent
          res.status(200).send({
            success: validPassword ? true : false,
            message: validPassword
              ? "Successfully signed in!"
              : "Invalid login or password.",
            dataDescription: "",
            data: securedUserResponseObject(data)
          });
        } else {
          res.status(401).json({
            success: validPassword ? true : false,
            message: validPassword
              ? "Successfully signed in!"
              : "Invalid login or password.",
            dataDescription: "",
            data: {
              user: null
            }
          });
        }
      }
    );

  } catch(error) {
    console.log(`An error occured in try block`, error);
    next(error);
  }
});

module.exports = router;
