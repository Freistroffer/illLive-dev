const express = require('express');
const router = express.Router();
const signup = require('./signup');
const signin = require('./signin');
const authenticate = require('./authenticate');
const signout = require('./signout');
const confirmAccount = require('./confirm-account');
const resendConfirmationEmail = require('./resend-confirmation-email');
const confirm24HourMealReset = require('./confirm24HourMealReset');

router.use('/signup', signup);
router.use('/signin', signin);
router.use('/authenticate', authenticate);
router.use('/signout', signout);
router.use('/confirm-account', confirmAccount);
router.use('/resend-confirmation-email', resendConfirmationEmail);
router.use('/confirm-24-hour-meal-reset', confirm24HourMealReset);

module.exports = router;
