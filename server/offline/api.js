const router = require('express').Router();
const signIn = require('./signin');

router.use('/api/auth/signin', signIn);

module.exports = router;
