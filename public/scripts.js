(function() {
  if (!String.prototype.trim) {
  	(function() {
    	// Make sure we trim BOM and NBSP
    	var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    	String.prototype.trim = function() {
      	return this.replace(rtrim, '');
    	};
  })();

  if (!Array.prototype.removeAt) {
  	console.log(`!Array.prototype.removeAt`);
  	(function() {
	 	Array.prototype.removeAt = function(array, index) {
			return array.slice(0, index - 1).concat(array.slice(index, array.length));
		};
  	})();
  }
}
})();
