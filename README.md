Environment is 'development' by default
Client side configuration at /src/configs
Server side configuration at /config

Actions occur such as sign in, and in App.js's componentDidUpdate(), 
based on the prop changes, certain behaviors take place, such as
show a loader, show a message, or routing. 